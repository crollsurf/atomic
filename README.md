# README #

Atomic is a WPF Windows desktop application that displays information about Elements and Sub-Atomic particles.  This project is part of a long term project inspired by the [Simulation hypothesis](https://en.wikipedia.org/wiki/Simulation_hypothesis) and a general interest in microbiology.
The primary purpose for making this code publicly available, is to demonstrate my .Net skills in WPF/MVVM, Entity Framework and Unit Testing.  This project is currently on hold while developing a Protein Data Bank file reader and viewer, to be incorporated into the Atomic application later.

Version: 1.0.1

![demo.png](https://bitbucket.org/repo/K75kgn/images/2166505013-demo.png)

For an example of MVVM 2 way binding, click one of the elements and then select the properties tab in the Element form.

![elementdemo.png](https://bitbucket.org/repo/K75kgn/images/3857910492-elementdemo.png)

## How do I get set up? ##

### Summary of set up ###
Once installed via download or clone select the Atomic.View.Wpf project and "'Set as StartUp Project" and run.  The NuGet package manager should be smart enough to install the required Nuget Packages (see Dependencies).  The application will take some time to start the first time.  Do not be concerned, the application will be seeding the database.
### Configuration ###
See Database configuration section below
### Dependencies ###
Other than the .NET Framework and an instance of SQL Server, the solution has the following dependencies.  All are documented in the packages folder of the individual projects and freely available on NuGet.

*   EntityFramework" version="6.1.3"
*   EntityFrameworkTesting" version="1.2.0"
*   EntityFrameworkTesting.FakeItEasy" version="1.2.0"
*   FakeItEasy" version="1.25.3"
*   CommonServiceLocator version="1.3"
*   DynamicDataDisplayWpf version="0.4.0"
*   EntityFramework version="6.1.3"
*   MahApps.Metro version="1.2.4.0"
*   MvvmLight version="5.2.0.0"
*   MvvmLightLibs version="5.2.0.0"
### Database configuration ###
Atomic requires a connection to an instance of SQL server, so it is likely that you will need to change the connection strings in the App.config files.  The current connection string assumes you have a default installation of SQL Server Express.
### How to run tests ###
Unit/Integration tests are already part of the solution but require a reference to FakeItEasy for mocking of services.
### Deployment instructions ###
Atomic is not currently ready for deployment.
## Who do I talk to? ##
Feel free to contact Robert Croll at robert at croll.me