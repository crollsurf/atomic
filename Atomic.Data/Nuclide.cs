﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace Atomic.Data
{
	[DebuggerDisplay("NuclideEntity : {ProtonCount}")]
	public class Nuclide
	{
		public Nuclide()
		{
		}

		public int Id { get; set; }
		public string Symbol { get; set; }
		public int ProtonCount { get; set; }
		public int NeutronCount { get; set; }
		public double Energy { get; set; }
		public string JPi { get; set; }
		public double Mass { get; set; }
		public string HalfLifeText { get; set; }
		public double HalfLifeSeconds { get; set; }
		public double Abundance { get; set; }
		public List<RadiationDecay> Decay { get; set; }

		public int? ElementId { get; set; }

		[ForeignKey("ElementId")]
		public Element Element { get; set; }

	}

}
