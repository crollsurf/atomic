﻿namespace Atomic.Data
{
	public enum RadiationDecayType
	{
		IT,
		BMinus,
		TwoBMinus,
		EC,
		EA,
		EP,
		E2P,
		TwoEC,
		E3P,
		N,
		TwoN,
		TwoNQuestionMark,
		BN,
		B2N,
		BNA,
		B3N,
		B4N,
		P,
		TwoP,
		A,
		BA,
		TwoA,
		B3A,
		TwelveC,
		FourteenC,
		TwentyO,
		TwentyFourNe,
		TwentyEightMg,
		Mg,
		Ne,
		ThirtyFourSi,
		SF,
		BF,
		EF,
		Unknown
    };

    public sealed class RadiationDecay
	{
		public RadiationDecay()
		{
		}

		public RadiationDecay(RadiationDecayType decayMode, string branchPercent)
        {
            DecayMode = decayMode;
            BranchPercent = branchPercent;
        }

		public int Id { get; set; }
		public RadiationDecayType DecayMode { get; set; }
		public string BranchPercent { get; set; }
    }
}
