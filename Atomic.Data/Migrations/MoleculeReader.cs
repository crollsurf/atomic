﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Atomic.Data.Migrations
{
    class MoleculeReader
	{
		internal static List<Molecule> ReadMolecules()
		{
			var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;
			var directoryName = Path.GetDirectoryName(absolutePath);

			List<Molecule> data = new List<Molecule>();
			List<string> headers = null;

			var lines = File.ReadAllLines(directoryName + @"\DataFiles\molecules.txt");
			Molecule currentMolecule = null;

			foreach (var line in lines)
			{
				var lineValues = line.Split('\t').Select(l => l.Trim()).ToList<string>();

				if (lineValues.Count == 5)
				{
					if (headers == null)
						headers = lineValues;
					else
					{
						if (string.IsNullOrEmpty(lineValues[0]))
						{
							//Add to current molecule
							currentMolecule.IsotopicSpecies += ", " + lineValues[2];
						}
						else
						{
							currentMolecule = new Molecule()
							{
								EmpiricalFormula = lineValues[0],
								Name = lineValues[1].Replace("\"", ""),
								IsotopicSpecies = lineValues[2],
								CANumber = lineValues[3]
							};

							data.Add(currentMolecule);
						}
					}
				}
			}

			return data;
		}
	}
}
