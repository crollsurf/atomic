﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Atomic.Data.Migrations
{
	class NuclideReader
	{
		internal static List<Nuclide> ReadNuclides(AtomicContext context)
		{
			List<Nuclide> nuclideData = ReadNuclideDataFile(context);
			List<Nuclide> nuclides = new List<Nuclide>();

			Nuclide previous = null;

			// The flat file has multiple records for a nuclide so 
			// need to group decays into a single nuclides
			foreach (var item in nuclideData)
			{
				if (previous == null)
				{
					nuclides.Add(item);
					previous = item;
				}
				else
				{
					if (item.Symbol == previous.Symbol && item.ProtonCount == previous.ProtonCount && item.NeutronCount == previous.NeutronCount && item.JPi == previous.JPi)
					{
						if (item.Decay != null) //Soemtimes we get a dup without decay!
							previous.Decay.Add(item.Decay.First());
					}
					else
					{
						nuclides.Add(item);
						previous = item;
					}
				}
			}

			var elements = context.Elements.ToList();

			foreach (var nuclide in nuclides)
			{
				Element element = elements.FirstOrDefault(e => e.Symbol == nuclide.Symbol);
				nuclide.ElementId = element?.Id;
			}

			return nuclides;
		}

		static List<Nuclide> ReadNuclideDataFile(AtomicContext context)
		{
			var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;
			var directoryName = Path.GetDirectoryName(absolutePath);

			List<Nuclide> data = new List<Nuclide>();
			List<string> headers = null;

			var lines = File.ReadAllLines(directoryName + @"\DataFiles\nudat2.txt");

			foreach (var line in lines)
			{
				var lineValues = line.Split('\t').Select(l => l.Trim()).ToList<string>();

				if (lineValues.Count == 14)
				{
					if (headers == null)
						headers = lineValues;
					else
						data.Add(CreateNuclide(lineValues, headers, context));
				}
			}

			return data;
		}

		static Nuclide CreateNuclide(List<string> line, List<string> headers, AtomicContext context)
		{
			Nuclide nuclide = new Nuclide();

			for (int i = 0; i < line.Count; i++)
				AddNuclideProperty(nuclide, line[i], headers[i], context);

			return nuclide;
		}

		static void AddNuclideProperty(Nuclide nuclide, string value, string header, AtomicContext context)
		{
			if (String.IsNullOrEmpty(value))
				return;

			switch (header)
			{
				case "A":
					nuclide.ProtonCount = int.Parse(value);
					break;
				case "Element":
					nuclide.Symbol = value;
					break;
				case "Z":
					nuclide.ProtonCount = int.Parse(value); ;
					break;
				case "N":
					nuclide.NeutronCount = int.Parse(value); ;
					break;
				case "Energy":
					nuclide.Energy = Double.Parse(value);
					break;
				case "JPi":
					nuclide.JPi = value;
					break;
				case "Mass Exc":
					if (value != "**********")
						nuclide.Mass = Double.Parse(value);
					break;
				case "T1/2 (txt)":
					nuclide.HalfLifeText = value;
					break;
				case "T1/2 (seconds)":
					if (value == "infinity")
						nuclide.HalfLifeSeconds = Double.MaxValue;
					else
						nuclide.HalfLifeSeconds = Double.Parse(value);
					break;
				case "Abund.":
					nuclide.Abundance = Double.Parse(value.Replace("%", ""));
					break;
				case "Dec Mode":
					nuclide.Decay = new List<RadiationDecay>();
					nuclide.Decay.Add(new RadiationDecay(ConvertToRadiationDecayType(value), ""));
					break;
				case "Branching (%)":
					var decay = nuclide.Decay.ToList().FirstOrDefault();
					if (decay != null)
						decay.BranchPercent = value;
					break;
			}
		}

		static RadiationDecayType ConvertToRadiationDecayType(string type)
		{
			switch (type)
			{
				case "IT":
					return RadiationDecayType.IT;
				case "B-":
					return RadiationDecayType.BMinus;
				case "2B-":
					return RadiationDecayType.TwoBMinus;
				case "EC":
					return RadiationDecayType.EC;
				case "EA":
					return RadiationDecayType.EA;
				case "EP":
					return RadiationDecayType.EP;
				case "E2P":
					return RadiationDecayType.E2P;
				case "2EC":
					return RadiationDecayType.TwoEC;
				case "E3P":
					return RadiationDecayType.E3P;
				case "N":
					return RadiationDecayType.N;
				case "2N":
					return RadiationDecayType.TwoN;
				case "2N?":
					return RadiationDecayType.TwoNQuestionMark;
				case "BN":
					return RadiationDecayType.BN;
				case "B2N":
					return RadiationDecayType.B2N;
				case "BNA":
					return RadiationDecayType.BNA;
				case "B3N":
					return RadiationDecayType.B3N;
				case "B4N":
					return RadiationDecayType.B4N;
				case "P":
					return RadiationDecayType.P;
				case "2P":
					return RadiationDecayType.TwoP;
				case "A":
					return RadiationDecayType.A;
				case "BA":
					return RadiationDecayType.BA;
				case "2A":
					return RadiationDecayType.TwoA;
				case "B3A":
					return RadiationDecayType.B3A;
				case "12C":
					return RadiationDecayType.TwelveC;
				case "14C":
					return RadiationDecayType.FourteenC;
				case "20O":
					return RadiationDecayType.TwelveC;
				case "24Ne":
					return RadiationDecayType.TwentyFourNe;
				case "28Mg":
					return RadiationDecayType.TwentyEightMg;
				case "Mg":
					return RadiationDecayType.Mg;
				case "Ne":
					return RadiationDecayType.Ne;
				case "34Si":
					return RadiationDecayType.ThirtyFourSi;
				case "SF":
					return RadiationDecayType.SF;
				case "BF":
					return RadiationDecayType.BF;
				case "EF":
					return RadiationDecayType.EF;
				default:
					return RadiationDecayType.Unknown;
			}
		}

	}
}
