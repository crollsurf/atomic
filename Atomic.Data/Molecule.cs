﻿using System.Diagnostics;

namespace Atomic.Data
{
	[DebuggerDisplay("LeptonEntity : {Name}")]
	public class Molecule
	{
		public Molecule()
		{
		}

		public int Id { get; set; }
		public string EmpiricalFormula { get; set; }
		public string Name { get; set; }
		public string IsotopicSpecies { get; set; } // Comma Delimited string of species
		public string CANumber { get; set; }
	}
}
