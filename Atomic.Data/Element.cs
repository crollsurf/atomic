﻿using System.Collections.Generic;
using System.Diagnostics;

namespace Atomic.Data
{
	[DebuggerDisplay("Element {Name}")]
	public class Element
	{
		public Element()
		{
		}

		public int Id { get; set; }
		public string Name { get; set; }
		public int Number { get; set; }
		public string Symbol { get; set; }
		public ICollection<Nuclide> Isotopes { get; set; }
		public int Group { get; set; }
		public string GroupOld { get; set; }
		public int Period { get; set; }
		public string NameGerman { get; set; }
		public string NameFrench { get; set; }
		public string NameSpanish { get; set; }
		public string NameItalian { get; set; }
		public string CasRegistryNumber { get; set; }
		public string Uses { get; set; }
		public double? EarthCrust { get; set; }
		public double? Seawater { get; set; }
		public double? HumanBody { get; set; }
		public string Color { get; set; }
		public int? YearDiscovered { get; set; }
		public double? RelativeAbundanceInSolarSystem { get; set; }
		public double? AbundanceInEarthCrust { get; set; }

		//Atomic data
		public double Mass { get; set; }
		public double Charge { get; set; }
		public double? MeltingPoint { get; set; }
		public double? BoilingPoint { get; set; }
		public double? Density { get; set; }
		public double? Electronegativity { get; set; }
		public double? IonizationPotentialFirst { get; set; }
		public double? IonizationPotentialSecond { get; set; }
		public double? IonizationPotentialThird { get; set; }
		public double? ElectronAffinity { get; set; } // 0 == <0
		public ICollection<int> OxidationStates { get; set; }
		public ICollection<int> IonsCommonlyFormed { get; set; }
		public string PredictedElectronConfiguration { get; set; }
		public string ObservedElectronConfiguration { get; set; }
		public double? AtomicRadius { get; set; }
		public double? IonicRadius { get; set; }
		public double? CovalentRadius { get; set; }
		public int? Radius2minusIon { get; set; }
		public int? Radius1minusIon { get; set; }
		public double? AtomicRadiusPm { get; set; }
		public double? Radius1plusIon { get; set; }
		public double? Radius2plusIon { get; set; }
		public double? Radius3plusIon { get; set; }
		public double? AtomicVolume { get; set; }
		public string CrystalStructure { get; set; }
		public double? ElectricalConductivity { get; set; }
		public double? SpecificHeat { get; set; }
		public double? HeatOfFusion { get; set; }
		public double? HeatOfVaporization { get; set; }
		public double? ThermalConductivity { get; set; }
		public string Structure { get; set; }
		public double? Hardness { get; set; }
		public string ReactionWithAir { get; set; }
		public string ReactionWithWater { get; set; }
		public string ReactionWith6MHCl { get; set; }
		public string ReactionWith15MHNO3 { get; set; }
		public string ReactionWith6MNaOH { get; set; }
		public string Hydride { get; set; }
		public string Oxide { get; set; }
		public string Chloride { get; set; }
		public double? Polarizability { get; set; }
		public int? HeatAtomization { get; set; }

	}
}
