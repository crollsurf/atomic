Empirical Formula	Name	Isotopic  Species	CA Number	Reference
AlCN	Aluminum isocyanide	AlCN	[61192-70-9]	Ziu02
AlCl	Aluminum monochloride	Al35Cl	[13595-81-8]	Cer87c
		Al37Cl		
AlF	Aluminum monofluoride	AlF	[13595-82-9]	Cer87c
CF+	Flouromethyliumylidene	CF+	[33412-11-2]	Neu06
CH	Methylidyne	CH	[3315-37-5]	"Ryd74,�McK40"
CHN	Hydrocyanic acid (hydrogen cyanide)	HCN	[74-90-8]	Sny71a
		H13CN		
		HC15N		
		DCN		
CHN	Hydroisocyanic acid (hydrogen isocyanide)	HNC	[6914-07-4]	Sny71
		H15NC		
		HN13C		
		DNC		
		D15NC		
CHNO	Isocyanic acid	HNCO	[75-13-8]	Sny71
		DNCO		
CHNS	Isothiocyanic acid	HNCS	[3129-90-6]	Fre79
CHO	Oxomethyl (formyl)	HCO	[2597-44-6]	Sny76
CHO+	Oxomethylium (formylium)	HCO+	[17030-74-9]	Buh70
		H13CO+		
		HC17O+		
		HC18O+		
		DCO+		
		D13CO+		
CHO+	Hydroxymethylidyne	HOC+	[60528-75-8]	"Woo83,�Ziu95a"
CHO2+	Hydroxyoxomethylium	HOCO+	[638-71-1]	Tha81
CHP	Phosphaethyne	HCP	[6829-52-3]	Ag�07
CHS+	Thiooxomethylium	HCS+	[59348-25-3]	Tha81
CH2	Methylene	CH2	[2465-56-7]	"Hol89,�Hol95"
CH2N+	Iminomethylium	HCNH+	[38263-97-7]	Ziu86a
CH2N	Methylene amidogen	CH2N	[15845-29-1]	Ohi94
CH2N2	Cyanamide	NH2CN	[420-04-2]	Tur75a
CH2O	Formaldehyde (methanal)	H2CO	[50-00-0]	Sny69
		H213CO		
		H2C18O		
		HDCO		
		D2CO		
CH2O2	Formic acid	HCOOH	[64-18-6]	"Zuc71,�Win75"
		H13COOH		
		HCOOD		
		DCOOH		
CH2S	Methanethial (thioformaldehyde)	H2CS	[865-36-1]	Sin73
		H213CS		
		H2C34S		
		HDCS		
		D2CS		
CH3	Methyl	CH3	[2229-07-4]	Feu00
CH3N	Methanimine	CH2NH	[2053-29-4]	God73
		13CH2NH		
CH3NO	Formamide	NH2CHO	[75-12-7]	Rub71
		NH213CHO		
CH3O+	Hydroxy methylium ion	H2COH+	[17691-31-5]	Ohi96
	(protonated formaldehyde)			
CH4O	Methanol (methyl alcohol)	CH3OH	[67-56-1]	Bal70
		13CH3OH		
		CH318OH		
		CH2DOH		
		CH3OD		
		CHD2OH		
CH4S	Methanethiol (methyl mercaptan)	CH3SH	[74-93-1]	Lin79
CH5N	Methanamine (methylamine)	CH3NH2	[74-89-5]	"Fou74a,�Kai74"
CMgN	Magnesium cyanide	MgCN	[74758-76-2]	Ziu95
CMgN	Magnesium isocyanide	24MgNC	[96491-22-4]	"Gue86,�Gue93"
		25MgNC		
		26MgNC		
CN	Cyanogen	CN	[2074-87-5]	"Jef70,�McK40"
		13CN		
		C15N		
CNNa	Sodium cyanide	NaCN	[143-33-9]	Tur94
CNSi	Silicon cyanide	SiCN	[29210-66-0]	Gu�00
CNSi	Silicon isocyanide	SiNC	[116430-49-0]	Gu�04
CO	Carbon monoxide	CO	[630-08-0]	Wil70
		13CO		
		C17O		
		C18O		
		13C18O		
		13C17O		
CO+	Carbon monoxide ion	CO+	[12144-04-6]	"Eri81,�Lat93"
COS	Carbon oxide sulfide (carbonyl sulfide)	OCS	[463-58-1]	Jef71
		OC34S		
		O13CS		
		18OCS		
CO2	Carbon dioxide	CO2	[124-38-9]	Jus96
CP	Carbon monophosphide	CP	[12326-85-1]	"Sai89,�Gue90"
CS	Carbon monosulfide	CS	[2944-05-0]	Lis75
		C33S		
		C34S		
		C36S		
		13CS		
		13CS34S		
CSi	Silicon monocarbide	SiC	[409-21-2]	Cer89
C2H	Ethynyl	C2H	[2122-48-7]	Tuc78
		13CCH		
		C13CH		
		C2D		
C2HN	Cyanomethylene	HCCN	[2612-62-6]	Gue91
C2HNO	Cyanoformaldehyde	CNCHO		Rem08
C2H2N	Cyanomethyl	CH2CN	[2932-82-3]	Irv88a
C2H2O	Ethanone (ketene)	CH2CO	[463-51-4]	Tur77
C2H3N	Acetonitrile (methyl cyanide)	CH3CN	[75-05-8]	Sol71
		13CH3CN		
		CH313CN		
		CH3C15N		
		CH2DCN		
C2H3N	Isocyanomethane (methyl isocyanide)	CH3NC	[593-75-9]	Cer88
C2H3N	Keteneimine (ethenimine)	CH2CNH	[17619-22-6]	Lov06a
C2H4O	Acetaldehyde (ethanal)	CH3CHO	[75-07-0]	Got73
C2H4O	Oxirane (ethylene oxide)	c-C2H4O	[75-21-8]	Dic97
C2H4O	Ethenol (vinyl alcohol)	a-CH2CHOH	[557-75-5]	Tur01
		s-CH2CHOH		
C2H4O2	Methyl ester formic acid (methyl formate)	CH3OCHO	[107-31-3]	Bro75
C2H4O2	Acetic acid	CH3COOH	[64-19-7]	Meh97
C2H4O2	Hydroxy acetaldehyde (glycolaldehyde)	CH2OHCHO	[141-46-8]	Hol00
C2H5NO	Acetamide	CH3CONH2	[60-35-5]	Hol06a
C2H6O	trans-Ethanol (ethyl alcohol)	t-CH3CH2OH	[64-17-5]	Zuc75
	gauche-Ethanol	g-CH3CH2OH		Pea96
C2H6O	Dimethyl ether (oxybismethane)	CH3OCH3	[115-10-6]	Sny74
C2H6O2	Ethylene glycol	HOCH2CH2OH	[107-21-1]	Hol02
C2O	Oxoethenylidene	CCO	[119754-08-4]	Ohi91
C2P	Phosphaethenylidene	CCP	[154474-31-4]	Hal08
C2S	Thioxoethenylidene	CCS	[109545-32-6]	Yam90
		CC34S		
C2Si	Silicon carbide (silacyclopropyne)	SiC2	[12071-27-1]	Tha84
		29SiC2		
		30SiC2		
		Si13CC		
C3	Tricarbon	C3	[175780-10-6]	Gie01
C3H	Cyclopropenylidyne	c-C3H	[16165-40-5]	Yam87a
		c-CC13CH		
C3H	Propenylidyne	l-C3H	[53590-28-6]	Tha85
C3HN	2-Propynenitrile (cyanoacetylene)	HCCCN	[1070-71-9]	Tur71
		H13CCCN		
		HC13CCN		
		HCC13CN		
		HCCC15N		
		DCCCN		
C3HN	"Ethyne, isocyanide"	HCCNC	[66723-45-3]	Kaw92
C3HN	"3-Imino-1,2-propadienylidene"	HNCCC	[76092-41-6]	Kaw92a
C3H2	Cyclopropenylidene	c-C3H2	[16165-40-5]	Mat85a
		c-H13CCCH		
		c-HC13CCH		
		c-C3HD		
C3H2	"1,2-Propadienylidene"	H2CCC	[60731-10-4]	Cer91
C3H2N+	Protonated 2-propynenitrile	CCCNH+	[76092-42-7]	Kaw94
C3H2O	2-Propynal	HCCCHO	[624-67-9]	Irv88
C3H2O	Cyclopropenone	c-C2H2O	[2966080-0]	Hol06
C3H3N	2-Propenenitrile (vinyl cyanide)	CH2CHCN	[107-13-1]	Gar75
		13CH2CHCN		
		CH213CHCN		
C3H4	1-Propyne (methyl acetylene)	CH3CCH	[74-99-7]	Sny71
		CH3C13CH		
		13CH3CCH		
		CH2DCCH		
		CH3CCD		
C3H4O	2-Propenal (acrolein)	CH2CHCHO	[107-02-8]	Hol04
C3H5N	Propanenitrile (ethyl cyanide)	CH3CH2CN	[107-12-0]	Joh77
		CH3CH213CN		
		CH313CH2CN		
		13CH3CH2CN		
C3H6	1-Propene (propylene)	CH2CHCH3	[115-07-1]	Mar-07
C3H6O	2-Propanone (acetone)	(CH3)2CO	[67-64-1]	"Com87,�Sny02"
C3H6O	2-Propanal	CH3CH2CHO	[123-38-6]	Hol04
C3N	Cyanoethynyl	CCCN	[62435-43-2]	Gue77
C3N-	Cyanoethynyl anion	CCCN-	[12373-53-4]	Tha08
C3O	"3-Oxo-1,2-propadienylidene"	CCCO	[11127-17-6]	Mat84
C3S	"3-Thioxo-1,2-propadienylidene"	CCCS	[109545-35-9]	Yam87
C3Si	"3-Silanetetrayl-1,2-propadienylidene (rhomboidal SiC3)"	SiC3	[184291-25-1]	App99
C4H	"1,3-Butadiynyl radical"	C4H	[53561-65-2]	Gue77
		13CCCCH		
		C13CCCH		
		CC13CCH		
		CCC13CH		
		C4D		
C4H-	"1,3-Butadiynyl anion"	C4H-	[59012-21-4]	Cer07
C4HN	"3-Cyano-1,2-propadienylidene"	HCCCCN	[847643-26-9]	Bro84
C4H2	Butatrienylidene	H2CCCC	[70571-89-0]	Bro84
C4H3N	2-Butynenitrile	CH3CCCN	[13752-78-8]	Bro84
C4H3N	"Cyanoallene (2,3-butadienenitrile)"	CH2CCHCN	[1001-56-5]	Lov06
C4Si	Silicon tetracarbide	SiC4	[144920-67-2]	Ohi89
		SiCCC13C		
C5H	"2,4-Pentadiynylidyne"	HCCCCC	[104602-63-3]	Cer86
C5HN	"2,4-Pentadiynenitrile (cyanobutadiyne)"	HCCCCCN	[59866-32-9]	Ave76
		H13CCCCCN		
		HC13CCCCN		
		HCC13CCCN		
		HCCC13CCN		
		HCCCC13CN		
		DCCCCCN		
C5H4	"1,3-Pentadiyne (methyl diacetylene)"	CH3C4H	[4911-55-1]	Wal84
C5N	"4-cyano-1,3-Butadiynylium"	C5N	[129066-48-4]	Gue98
C6H	"1,3,5-hexatriynyl"	HCCCCCC	[88053-50-3]	Suz86
C6H-	"1,3,5-hexatriynyl anion"	H-CCCCCC	[169141-81-1]	McC06
C6H2	"1,3,5-Hexatriyne"	HCCCCCCH	[3161-99-7]	Cer01a
C6H2	"1,2,3,4,5-hexapentaenylidene"	H2CCCCCC	[129066-05-3]	Lan97
C6H3N	"2,4-Hexadiynenitrile"	CH3C4CN	[66486-69-9]	Sny06
C7H	"2,4,6-Heptatriynylidyne"	HCCCCCCC	[129066-03-1]	Gue97
C7HN	"2,4,6-heptatriynenitrile"	HC7N	[65937-22-6]	Kro78
C7H4	Methyltriacetylene	CH3C6H	[66486-68-8]	Rem06
C8H	"1,3,5,7-Octatetraynyl"	HC8a	[88053-51-4]	Cer96
C8H-	"1,3,5,7-Octatetraynyl anion"	HC8-	[149673-52-9]	Br�07
C9HN	"2,4,6,8-nonatetraynenitrile"	HC9N	[67483-72-1]	Bro78
C11HN	"2,4,6,8,10-undecapentaynenitrile"	HC11N	[78950-25-1]	Bel97
ClH	Hydrochloric acid	H35Cl	[7647-01-0]	Sch95
		H37Cl		
ClK	Potassium chloride	K35Cl	[7447-40-7]	Cer87c
		K37Cl		
ClNa	Sodium chloride	Na35Cl	[7647-14-5]	Cer87c
		Na37Cl		
FH	Hydrogen fluoride	HF	[7664-39-3]	Neu97
FeO	Iron monoxide	FeO	[1345-25-1]	Wal02
HLi	Lithium Hydride	7LiH	[7580-67-8]	Com98
HNO	Nitrosyl hydride	HNO	[14332-28-6]	"Uil77,�Sny93"
HN2+	Hydrodinitrogen(1+) (diazenylium)	N2H+	[12357-66-3]	"Tur74,�Gre74"
		15NNH+		
		N15NH+		
		N2D+		
HO	Hydroxyl	OH	[3352-57-6]	Wei63
		17OH		
		18OH		
HS	Mercapto (thiohydroxyl)	SH	[13940-21-1]	Yam00
H2N	Amidogen	NH2	[13770-40-6]	vDi93
H2O	Water	H2O	[7732-18-5]	Che69
		H218O		
		HDO		
		D2O		
H2S	Hydrogen sulfide	H2S	[7783-06-4]	Tha72
		H234S		
		HDS		
H3+	Hydrogen ion	H2D+	[28132-48-1]	Sta99
		HD2+		
H3N	Ammonia	NH3	[7664-41-7]	Che68
		15NH3		
		NH2D		
		NHD2		
		ND3		
H3O+	Oxonium hydride	H3O+	[28637-38-9]	Hol86
H4Si	Silane	SiH4	[7803-62-5]	Kea93
NO	Nitrogen oxide (nitric oxide)	NO	[10102-43-9]	Lis78a
NP	Phosphorous nitride	PN	[17739-47-8]	Tur87b
NS	Nitrogen sulfide (nitric sulfide)	NS	[12033-56-6]	Got75
		N34S		
NSi	Silicon mononitride	SiN	[12033-60-2]	Tur92
N2O	Nitrogen oxide (nitrous oxide)	N2O	[10024-97-2]	Ziu94
OP	Phosphorous oxide	PO	[14452-66-5]	Ten07
OS	Sulfur monoxide	SO	[13827-32-2]	Got73b
		33SO		
		34SO		
		S18O		
OS+	"Sulfur(1+), oxo"	SO+	[54724-05-9]	Tur92a
OSi	Silicon monoxide	SiO	[113443-18-8]	Wil71
		29SiO		
		30SiO		
O2	Oxygen	O2	[7782-44-7]	Lar07
O2S	Sulfur dioxide	SO2	[7446-09-5]	Sny75a
		33SO2		
		34SO2		
		OS18O		
		OS17O		
SSi	Silicon monosulfide	SiS	[25423-24-9]	Mor75
		Si33S		
		Si34S		
		29SiS		
		30SiS		
				
				
				
