﻿using System.Diagnostics;

namespace Atomic.Data
{
	public enum QuarkFlavor { Up, Down, Charm, Strange, Top, Bottom, AntiUp, AntiDown, AntiCharm, AntiStrange, AntiTop, AntiBottom };

	[DebuggerDisplay("QuarkEntity : {Symbol}")]
	public class Quark
	{
		public Quark()
		{
		}

		public int Id { get; set; }
		public string Symbol { get; set; }
		public double MinimumMassMev { get; set; }
		public double MaximumMassMev { get; set; }
		public double Charge { get; set; }
		public QuarkFlavor Flavor { get; set; }
		public double Spin { get { return 0.5; } }
		public double BaryonNumber { get { return 1d / 3d; } }

	}
}
