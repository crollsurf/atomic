﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace Atomic.Data
{
	public enum HadronFamily { Baryon, Meson };

	[DebuggerDisplay("HadronEntity : {Name}")]
	public class Hadron
	{
		public Hadron()
		{
		}

		public int Id { get; set; }
		public string Name { get; set; }
		public string Symbol { get; set; }
		public HadronFamily Family { get; set; }
        public string Quark1Symbol { get; set; }
        public string Quark2Symbol { get; set; }
        public string Quark3Symbol { get; set; }
		public double MinimumMassMev { get; set; }
		public double MaximumMassMev { get; set; }
		public double Isospin { get; set; }
		public double TotalAngularMomentum { get; set; }
		public double Charge { get; set; }
		public int Strangeness { get; set; }
		public int Charm { get; set; }
		public int Bottomness { get; set; }
		public string MeanLifetime { get; set; }
		public string DecaysTo { get; set; }

    }
}
