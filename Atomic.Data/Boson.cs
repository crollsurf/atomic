﻿using System.Diagnostics;

namespace Atomic.Data
{
    [DebuggerDisplay("BosonEntity : {Symbol}")]
    public class Boson
    {
        public Boson()
        {
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Symbol { get; set; }
        public string Anitparticle { get; set; }
        public double Charge { get; set; }
        public double Spin { get; set; }
        public double Mass { get; set; }
        public string InteractionMediated { get; set; }

    }
}
