﻿using System.Data.Entity;

namespace Atomic.Data
{
	public class AtomicContext : DbContext
	{
		public AtomicContext(): base("Atomic")
        {
            this.Configuration.LazyLoadingEnabled = true;
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<AtomicContext, Atomic.Data.Migrations.Configuration>("Atomic"));
        }

		public virtual DbSet<Element> Elements { get; set; }
		public virtual DbSet<Hadron> Hadrons { get; set; }
		public virtual DbSet<Lepton> Leptons { get; set; }
		public virtual DbSet<Nuclide> Nuclides { get; set; }
        public virtual DbSet<Quark> Quarks { get; set; }
        public virtual DbSet<Boson> Bosons { get; set; }
        public virtual DbSet<RadiationDecay> RadiationDecays { get; set; }
		public virtual DbSet<Molecule> Molecules { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
			//one-to-many 
			modelBuilder.Entity<Nuclide>()
						.HasOptional<Element>(e => e.Element)
						.WithMany(e => e.Isotopes)
						.HasForeignKey(n => n.ElementId);

		}

	}
}

