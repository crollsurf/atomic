﻿using System.Diagnostics;

namespace Atomic.Data
{
	public enum LeptonFlavor { Electron, Muon, Tau, ElectronNeutrino, MuonNeutrino, TauNeutrino, Positron, AntiMuon, AntiTau, ElectronAntineutrino, MuonAntineutrino, TauAntineutrino };

	[DebuggerDisplay("LeptonEntity : {Name}")]
	public class Lepton
	{
		public Lepton()
		{
		}

		public int Id { get; set; }
		public LeptonFlavor Flavor { get; set; }
		public string Symbol { get; set; }
		public double Charge { get; set; }
		public double Spin { get; set; }
		public int ElectronicNumber { get; set; }
		public int MuonicNumber { get; set; }
		public int TauonicNumber { get; set; }
		public double Mass { get; set; }
		public string Lifetime { get; set; }
		public string CommonDecay { get; set; }

	}
}
