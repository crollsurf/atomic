﻿using Atomic.Data.WebService.Contracts;
using System.Collections.Generic;

namespace Atomic.Data.WebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class AtomicService : IAtomicService
	{
        AtomicContext _context;

        public AtomicService()
        {
        }

        public AtomicService(AtomicContext context)
        {
            _context = context;
        }

        AtomicContext GetContext()
        {
            return (_context != null) ? _context : new AtomicContext();
        }

        public Contracts.Element GetElement(string symbol)
		{
            using (AtomicContext ctx = GetContext())
            {
                return Cache.ElementCache.GetElement(symbol, ctx);
            }
		}

		public IList<ElementListItem> GetElementList()
		{
            using (AtomicContext ctx = GetContext())
            {
                return Cache.ElementCache.GetElementSplitItems(ctx);
            }
		}

		public Contracts.Hadron GetHadron(string symbol)
		{
            using (AtomicContext ctx = GetContext())
            {
                return Cache.HadronCache.GetHadron(symbol, ctx);
            }
		}

		public IList<HadronListItem> GetHadronList()
		{
            using (AtomicContext ctx = GetContext())
            {
                return Cache.HadronCache.GetHadronSplitItems(ctx);
            }
		}

		public Contracts.Lepton GetLepton(string symbol)
		{
            using (AtomicContext ctx = GetContext())
            {
                return Cache.LeptonCache.GetLepton(symbol, ctx);
            }
		}

		public IList<Contracts.Lepton> GetLeptonList()
		{
            using (AtomicContext ctx = GetContext())
            {
                return Cache.LeptonCache.GetAllLeptons(ctx);
            }
		}

		public Contracts.Molecule GetMolecule(string name)
		{
            using (AtomicContext ctx = GetContext())
            {
                return Cache.MoleculeCache.GetMolecule(name, ctx);
            }
		}

		public IList<string> GetMoleculeList()
		{
            using (AtomicContext ctx = GetContext())
            {
                return Cache.MoleculeCache.GetAllMolecules(ctx);
            }
		}

		public Contracts.Quark GetQuark(string symbol)
		{
            using (AtomicContext ctx = GetContext())
            {
                return Cache.QuarkCache.GetQuark(symbol, ctx);
            }
		}

		public IList<Contracts.Quark> GetQuarkList()
		{
            using (AtomicContext ctx = GetContext())
            {
                return Cache.QuarkCache.GetAllQuarks(ctx);
            }
		}

		public IList<Contracts.Nuclide> GetNulcides(string symbol)
		{
            using (AtomicContext ctx = GetContext())
            {
                return Cache.NuclideCache.GetNuclides(symbol, ctx);
            }
		}

	}
}
