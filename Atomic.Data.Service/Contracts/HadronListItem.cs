﻿using System.Runtime.Serialization;

namespace Atomic.Data.WebService.Contracts
{
	[DataContract(Namespace = "urn:Simulation.Atomic.Data/Schemas/2016/1")]
	public class HadronListItem
	{
		[DataMember]
		public int Id { get; set; }
		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public string Symbol { get; set; }
		[DataMember]
		public string Family { get; set; }
	}
}
