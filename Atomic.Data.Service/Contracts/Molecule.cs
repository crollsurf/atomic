﻿using System.Runtime.Serialization;

namespace Atomic.Data.WebService.Contracts
{
	[DataContract(Namespace = "urn:Simulation.Atomic.Data/Schemas/2016/1")]
	public class Molecule
	{
		[DataMember]
		public int Id { get; set; }
		[DataMember]
		public string EmpiricalFormula { get; set; }
		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public string IsotopicSpecies { get; set; } // Comma Delimited string of species
		[DataMember]
		public string CANumber { get; set; }

	}
}
