﻿using System.Runtime.Serialization;

namespace Atomic.Data.WebService.Contracts
{
	[DataContract(Namespace = "urn:Simulation.Atomic.Data/Schemas/2016/1")]
	public class Hadron
	{
		[DataMember]
		public int Id { get; set; }
		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public string Symbol { get; set; }
		[DataMember]
		public string Family { get; set; }
		[DataMember]
		public double MinimumMassMev { get; set; }
		[DataMember]
		public double MaximumMassMev { get; set; }
		[DataMember]
		public double Isospin { get; set; }
		[DataMember]
		public double TotalAngularMomentum { get; set; }
		[DataMember]
		public double Charge { get; set; }
		[DataMember]
		public int Strangeness { get; set; }
		[DataMember]
		public int Charm { get; set; }
		[DataMember]
		public int Bottomness { get; set; }
		[DataMember]
		public string MeanLifetime { get; set; }
		[DataMember]
		public string DecaysTo { get; set; }

	}
}
