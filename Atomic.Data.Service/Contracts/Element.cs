﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Atomic.Data.WebService.Contracts
{
	[DataContract(Namespace = "urn:Simulation.Atomic.Data/Schemas/2016/1")]
	public class Element
	{
		[DataMember]
		public int Id { get; set; }
		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public int Number { get; set; }
		[DataMember]
		public string Symbol { get; set; }
		[DataMember]
		public int Group { get; set; }
		[DataMember]
		public string GroupOld { get; set; }
		[DataMember]
		public int Period { get; set; }
		[DataMember]
		public string NameGerman { get; set; }
		[DataMember]
		public string NameFrench { get; set; }
		[DataMember]
		public string NameSpanish { get; set; }
		[DataMember]
		public string NameItalian { get; set; }
		[DataMember]
		public string CasRegistryNumber { get; set; }
		[DataMember]
		public string Uses { get; set; }
		[DataMember]
		public double? EarthCrust { get; set; }
		[DataMember]
		public double? Seawater { get; set; }
		[DataMember]
		public double? HumanBody { get; set; }
		[DataMember]
		public string Color { get; set; }
		[DataMember]
		public int? YearDiscovered { get; set; }
		[DataMember]
		public double? RelativeAbundanceInSolarSystem { get; set; }
		[DataMember]
		public double? AbundanceInEarthCrust { get; set; }

		//Atomic data
		[DataMember]
		public double Mass { get; set; }
		[DataMember]
		public double Charge { get; set; }
		[DataMember]
		public double? MeltingPoint { get; set; }
		[DataMember]
		public double? BoilingPoint { get; set; }
		[DataMember]
		public double? Density { get; set; }
		[DataMember]
		public double? Electronegativity { get; set; }
		[DataMember]
		public double? IonizationPotentialFirst { get; set; }
		[DataMember]
		public double? IonizationPotentialSecond { get; set; }
		[DataMember]
		public double? IonizationPotentialThird { get; set; }
		[DataMember]
		public double? ElectronAffinity { get; set; } // 0 == <0
		[DataMember]
		public ICollection<int> OxidationStates { get; set; }
		[DataMember]
		public ICollection<int> IonsCommonlyFormed { get; set; }
		[DataMember]
		public string PredictedElectronConfiguration { get; set; }
		[DataMember]
		public string ObservedElectronConfiguration { get; set; }
		[DataMember]
		public double? AtomicRadius { get; set; }
		[DataMember]
		public double? IonicRadius { get; set; }
		[DataMember]
		public double? CovalentRadius { get; set; }
		[DataMember]
		public int? Radius2minusIon { get; set; }
		[DataMember]
		public int? Radius1minusIon { get; set; }
		[DataMember]
		public double? AtomicRadiusPm { get; set; }
		[DataMember]
		public double? Radius1plusIon { get; set; }
		[DataMember]
		public double? Radius2plusIon { get; set; }
		[DataMember]
		public double? Radius3plusIon { get; set; }
		[DataMember]
		public double? AtomicVolume { get; set; }
		[DataMember]
		public string CrystalStructure { get; set; }
		[DataMember]
		public double? ElectricalConductivity { get; set; }
		[DataMember]
		public double? SpecificHeat { get; set; }
		[DataMember]
		public double? HeatOfFusion { get; set; }
		[DataMember]
		public double? HeatOfVaporization { get; set; }
		[DataMember]
		public double? ThermalConductivity { get; set; }
		[DataMember]
		public string Structure { get; set; }
		[DataMember]
		public double? Hardness { get; set; }
		[DataMember]
		public string ReactionWithAir { get; set; }
		[DataMember]
		public string ReactionWithWater { get; set; }
		[DataMember]
		public string ReactionWith6MHCl { get; set; }
		[DataMember]
		public string ReactionWith15MHNO3 { get; set; }
		[DataMember]
		public string ReactionWith6MNaOH { get; set; }
		[DataMember]
		public string Hydride { get; set; }
		[DataMember]
		public string Oxide { get; set; }
		[DataMember]
		public string Chloride { get; set; }
		[DataMember]
		public double? Polarizability { get; set; }
		[DataMember]
		public int? HeatAtomization { get; set; }

	}
}
