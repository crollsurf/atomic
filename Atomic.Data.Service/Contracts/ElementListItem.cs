﻿using System.Runtime.Serialization;

namespace Atomic.Data.WebService.Contracts
{
	[DataContract(Namespace = "urn:Simulation.Atomic.Data/Schemas/2016/1")]
	public class ElementListItem
	{
		[DataMember]
		public int Id { get; set; }
		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public int Number { get; set; }
		[DataMember]
		public string Symbol { get; set; }
		[DataMember]
		public int Group { get; set; }
		[DataMember]
		public int Period { get; set; }
	}
}
