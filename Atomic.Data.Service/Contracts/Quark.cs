﻿using System.Runtime.Serialization;

namespace Atomic.Data.WebService.Contracts
{
    [DataContract(Namespace = "urn:Simulation.Atomic.Data/Schemas/2016/1")]
    public class Quark
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Symbol { get; set; }
        [DataMember]
        public double MinimumMassMev { get; set; }
        [DataMember]
        public double MaximumMassMev { get; set; }
        [DataMember]
        public double Charge { get; set; }
        [DataMember]
        public string Flavor { get; set; }
        [DataMember]
        public double Spin { get; set; }
        [DataMember]
        public double BaryonNumber { get; set; }

    }
}
