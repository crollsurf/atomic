﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Atomic.Data.WebService.Contracts
{
	[DataContract(Namespace = "urn:Simulation.Atomic.Data/Schemas/2016/1")]
	public class Nuclide
	{
		[DataMember]
		public int Id { get; set; }
		[DataMember]
		public string Symbol { get; set; }
		[DataMember]
		public int ProtonCount { get; set; }
		[DataMember]
		public int NeutronCount { get; set; }
		[DataMember]
		public double Energy { get; set; }
		[DataMember]
		public string JPi { get; set; }
		[DataMember]
		public double Mass { get; set; }
		[DataMember]
		public string HalfLifeText { get; set; }
		[DataMember]
		public double HalfLifeSeconds { get; set; }
		[DataMember]
		public double Abundance { get; set; }
		[DataMember]
		public IList<RadiationDecay> Decay { get; set; }
		[DataMember]
		public int? ElementId { get; set; }

	}
}
