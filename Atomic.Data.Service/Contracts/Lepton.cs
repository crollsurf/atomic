﻿using System.Runtime.Serialization;

namespace Atomic.Data.WebService.Contracts
{
	[DataContract(Namespace = "urn:Simulation.Atomic.Data/Schemas/2016/1")]
	public class Lepton
	{
		[DataMember]
		public int Id { get; set; }
		[DataMember]
		public string Flavor { get; set; }
		[DataMember]
		public string Symbol { get; set; }
		[DataMember]
		public double Charge { get; set; }
		[DataMember]
		public double Spin { get; set; }
		[DataMember]
		public int ElectronicNumber { get; set; }
		[DataMember]
		public int MuonicNumber { get; set; }
		[DataMember]
		public int TauonicNumber { get; set; }
		[DataMember]
		public double Mass { get; set; }
		[DataMember]
		public string Lifetime { get; set; }
		[DataMember]
		public string CommonDecay { get; set; }
	}
}
