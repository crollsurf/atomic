﻿using System.Runtime.Serialization;

namespace Atomic.Data.WebService.Contracts
{
	[DataContract(Namespace = "urn:Simulation.Atomic.Data/Schemas/2016/1")]
	public class RadiationDecay
	{
		[DataMember]
		public int Id { get; set; }
		[DataMember]
		public string DecayMode { get; set; }
		[DataMember]
		public string BranchPercent { get; set; }
	}
}
