﻿using System.Collections.Generic;
using System.ServiceModel;
using Atomic.Data.WebService.Contracts;

namespace Atomic.Data.WebService
{
	[ServiceContract]
	public interface IAtomicService
	{
		[OperationContract]
		IList<ElementListItem> GetElementList();

		[OperationContract]
		Contracts.Element GetElement(string symbol);

		[OperationContract]
		IList<HadronListItem> GetHadronList();

		[OperationContract]
		Contracts.Hadron GetHadron(string symbol);

		[OperationContract]
		IList<Contracts.Lepton> GetLeptonList();

		[OperationContract]
		Contracts.Lepton GetLepton(string symbol);

		[OperationContract]
		IList<string> GetMoleculeList();

		[OperationContract]
		Contracts.Molecule GetMolecule(string name);

		[OperationContract]
		IList<Contracts.Quark> GetQuarkList();

		[OperationContract]
		Contracts.Quark GetQuark(string symbol);

		[OperationContract]
		IList<Contracts.Nuclide> GetNulcides(string symbol);

	}

}
