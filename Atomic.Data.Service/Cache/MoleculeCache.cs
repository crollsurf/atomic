﻿using System.Collections.Generic;
using System.Linq;

namespace Atomic.Data.WebService.Cache
{
    class MoleculeCache
    {
        static IList<Contracts.Molecule> Molecules;
        static IList<string> MoleculeNames;

        internal static Contracts.Molecule GetMolecule(string name, AtomicContext ctx)
        {
            if (Molecules == null) CreateAllMolecules(ctx);
            return Molecules.FirstOrDefault(e => e.Name == name);
        }

        internal static IList<string> GetAllMolecules(AtomicContext ctx)
        {
            if (Molecules == null) CreateAllMolecules(ctx);
            return MoleculeNames;
        }

        static void CreateAllMolecules(AtomicContext ctx)
        {
            if (Molecules == null)
            {
                Molecules = new List<Contracts.Molecule>();
                MoleculeNames = new List<string>();

                foreach (var molecule in ctx.Molecules)
                {
                    Molecules.Add(new Contracts.Molecule()
                    {
                        CANumber = molecule.CANumber,
                        EmpiricalFormula = molecule.EmpiricalFormula,
                        Id = molecule.Id,
                        IsotopicSpecies = molecule.IsotopicSpecies,
                        Name = molecule.Name
                    });

                    MoleculeNames.Add(molecule.Name);
                }
            }
        }
    }
}
