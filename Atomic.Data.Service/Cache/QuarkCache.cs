﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Atomic.Data.WebService.Cache
{
    class QuarkCache
    {
        static IList<Contracts.Quark> Quarks;

        internal static Contracts.Quark GetQuark(string symbol, AtomicContext ctx)
        {
            if (Quarks == null) CreateAllQuarks(ctx);
            return Quarks.FirstOrDefault(e => e.Symbol == symbol);
        }

        internal static IList<Contracts.Quark> GetAllQuarks(AtomicContext ctx)
        {
            if (Quarks == null) CreateAllQuarks(ctx);
            return Quarks;
        }

        static void CreateAllQuarks(AtomicContext ctx)
        {
            if (Quarks == null)
            {
                Quarks = new List<Contracts.Quark>();

                foreach (var quark in ctx.Quarks)
                {
                    Quarks.Add(new Contracts.Quark()
                    {
                        Flavor = Enum.GetName(quark.Flavor.GetType(), quark.Flavor),
                        Charge = quark.Charge,
                        Id = quark.Id,
                        MaximumMassMev = quark.MaximumMassMev,
                        MinimumMassMev = quark.MinimumMassMev,
                        Symbol = quark.Symbol
                    });
                }
            }
        }
    }
}
