﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atomic.Data.WebService.Contracts;

namespace Atomic.Data.WebService.Cache
{
    class HadronCache
    {
        static IList<Contracts.Hadron> Hadrons;
        static IList<HadronListItem> HadronListItems;

        internal static Contracts.Hadron GetHadron(string symbol, AtomicContext ctx)
        {
            if (Hadrons == null) CreateAllHadrons(ctx);
            return Hadrons.FirstOrDefault(e => e.Symbol == symbol);
        }

        internal static IList<HadronListItem> GetHadronSplitItems(AtomicContext ctx)
        {
            if (HadronListItems == null) CreateAllHadrons(ctx);
            return HadronListItems;
        }

        static void CreateAllHadrons(AtomicContext ctx)
        {
            if (Hadrons == null)
            {
                Hadrons = new List<Contracts.Hadron>();
                HadronListItems = new List<Contracts.HadronListItem>();

                foreach (var hadron in ctx.Hadrons)
                {
                    Hadrons.Add(new Contracts.Hadron()
                    {
                        Bottomness = hadron.Bottomness,
                        Charge = hadron.Charge,
                        Charm = hadron.Charm,
                        Symbol = hadron.Symbol,
                        Name = hadron.Name,
                        DecaysTo = hadron.DecaysTo,
                        Family = Enum.GetName(hadron.Family.GetType(), hadron.Family),
                        Id = hadron.Id,
                        Isospin = hadron.Isospin,
                        MaximumMassMev = hadron.MaximumMassMev,
                        MeanLifetime = hadron.MeanLifetime,
                        MinimumMassMev = hadron.MinimumMassMev,
                        Strangeness = hadron.Strangeness,
                        TotalAngularMomentum = hadron.TotalAngularMomentum
                    });

                    HadronListItems.Add(new HadronListItem()
                    {
                        Id = hadron.Id,
                        Family = Enum.GetName(hadron.Family.GetType(), hadron.Family),
                        Name = hadron.Name,
                        Symbol = hadron.Symbol
                    });
                }
            }
        }
    }
}
