﻿using System.Collections.Generic;
using System.Linq;
using Atomic.Data.WebService.Contracts;

namespace Atomic.Data.WebService.Cache
{
    class ElementCache
    {
        static IList<Contracts.Element> Elements;
        static IList<ElementListItem> ElementListItems;

        internal static Contracts.Element GetElement(string symbol, AtomicContext ctx)
        {
            if (Elements == null) CreateAllContractElements(ctx);
            return Elements.FirstOrDefault(e => e.Symbol == symbol);
        }

        internal static IList<ElementListItem> GetElementSplitItems(AtomicContext ctx)
        {
            if (ElementListItems == null) CreateAllContractElements(ctx);
            return ElementListItems;
        }

        static void CreateAllContractElements(AtomicContext ctx)
        {
            if (Elements == null)
            {
                Elements = new List<Contracts.Element>();
                ElementListItems = new List<ElementListItem>();

                foreach (var element in ctx.Elements)
                {
                    Elements.Add(new Contracts.Element()
                    {
                        AbundanceInEarthCrust = element.AbundanceInEarthCrust,
                        AtomicRadius = element.AtomicRadius,
                        AtomicRadiusPm = element.AtomicRadiusPm,
                        AtomicVolume = element.AtomicVolume,
                        BoilingPoint = element.BoilingPoint,
                        CasRegistryNumber = element.CasRegistryNumber,
                        Charge = element.Charge,
                        Chloride = element.Chloride,
                        Color = element.Color,
                        CovalentRadius = element.CovalentRadius,
                        CrystalStructure = element.CrystalStructure,
                        Density = element.Density,
                        EarthCrust = element.EarthCrust,
                        ElectricalConductivity = element.ElectricalConductivity,
                        ElectronAffinity = element.ElectronAffinity,
                        Electronegativity = element.Electronegativity,
                        Group = element.Group,
                        GroupOld = element.GroupOld,
                        Hardness = element.Hardness,
                        HeatAtomization = element.HeatAtomization,
                        HeatOfFusion = element.HeatOfFusion,
                        HeatOfVaporization = element.HeatOfVaporization,
                        HumanBody = element.HumanBody,
                        Hydride = element.Hydride,
                        Id = element.Id,
                        IonicRadius = element.IonicRadius,
                        IonizationPotentialFirst = element.IonizationPotentialFirst,
                        IonizationPotentialSecond = element.IonizationPotentialSecond,
                        IonizationPotentialThird = element.IonizationPotentialThird,
                        IonsCommonlyFormed = element.IonsCommonlyFormed,
                        Mass = element.Mass,
                        MeltingPoint = element.MeltingPoint,
                        Name = element.Name,
                        NameFrench = element.NameFrench,
                        NameGerman = element.NameGerman,
                        NameItalian = element.NameItalian,
                        NameSpanish = element.NameSpanish,
                        Number = element.Number,
                        ObservedElectronConfiguration = element.ObservedElectronConfiguration,
                        OxidationStates = element.OxidationStates,
                        Oxide = element.Oxide,
                        Period = element.Period,
                        Polarizability = element.Polarizability,
                        PredictedElectronConfiguration = element.PredictedElectronConfiguration,
                        Radius1minusIon = element.Radius1minusIon,
                        Radius1plusIon = element.Radius1plusIon,
                        Radius2minusIon = element.Radius2minusIon,
                        Radius2plusIon = element.Radius2plusIon,
                        Radius3plusIon = element.Radius3plusIon,
                        ReactionWith15MHNO3 = element.ReactionWith15MHNO3,
                        ReactionWith6MHCl = element.ReactionWith6MHCl,
                        ReactionWith6MNaOH = element.ReactionWith6MNaOH,
                        ReactionWithAir = element.ReactionWithAir,
                        ReactionWithWater = element.ReactionWithWater,
                        RelativeAbundanceInSolarSystem = element.RelativeAbundanceInSolarSystem,
                        Seawater = element.Seawater,
                        SpecificHeat = element.SpecificHeat,
                        Structure = element.Structure,
                        Symbol = element.Symbol,
                        ThermalConductivity = element.ThermalConductivity,
                        Uses = element.Uses,
                        YearDiscovered = element.YearDiscovered
                    });

                    ElementListItems.Add(new ElementListItem()
                    {
                        Group = element.Group,
                        Id = element.Id,
                        Name = element.Name,
                        Number = element.Number,
                        Period = element.Period,
                        Symbol = element.Symbol
                    });
                }
            }
        }
    }
}
