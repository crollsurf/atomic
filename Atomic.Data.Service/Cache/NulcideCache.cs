﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Atomic.Data.WebService.Cache
{
    class NuclideCache
    {
        static IList<Contracts.Nuclide> Nuclides;

        internal static IList<Contracts.Nuclide> GetNuclides(string symbol, AtomicContext ctx)
        {
            if (Nuclides == null) CreateAllNuclides(ctx);
            return Nuclides.Where(e => e.Symbol == symbol).ToList();
        }

        static void CreateAllNuclides(AtomicContext ctx)
        {
            if (Nuclides == null)
            {
                Nuclides = new List<Contracts.Nuclide>();

                foreach (var nuclide in ctx.Nuclides)
                {
                    Nuclides.Add(new Contracts.Nuclide()
                    {
                        Symbol = nuclide.Symbol,
                        Abundance = nuclide.Abundance,
                        Decay = ConvertDecayList(nuclide.Decay),
                        ElementId = nuclide.ElementId,
                        Energy = nuclide.Energy,
                        HalfLifeSeconds = nuclide.HalfLifeSeconds,
                        HalfLifeText = nuclide.HalfLifeText,
                        Id = nuclide.Id,
                        JPi = nuclide.JPi,
                        Mass = nuclide.Mass,
                        NeutronCount = nuclide.NeutronCount,
                        ProtonCount = nuclide.ProtonCount
                    });
                }
            }
        }

        static IList<Contracts.RadiationDecay> ConvertDecayList(IList<RadiationDecay> decayData)
        {
            IList<Contracts.RadiationDecay> decays = new List<Contracts.RadiationDecay>();

            if (decayData != null)
            {
                foreach (var decay in decayData)
                {
                    decays.Add(new Contracts.RadiationDecay()
                    {
                        BranchPercent = decay.BranchPercent,
                        DecayMode = Enum.GetName(decay.DecayMode.GetType(), decay.DecayMode)
                    });
                }
            }

            return decays;
        }
    }
}
