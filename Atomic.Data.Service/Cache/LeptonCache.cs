﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Atomic.Data.WebService.Cache
{
    class LeptonCache
    {
        static IList<Contracts.Lepton> Leptons;

        internal static Contracts.Lepton GetLepton(string symbol, AtomicContext ctx)
        {
            if (Leptons == null) CreateAllLeptons(ctx);
            return Leptons.FirstOrDefault(e => e.Symbol == symbol);
        }

        internal static IList<Contracts.Lepton> GetAllLeptons(AtomicContext ctx)
        {
            if (Leptons == null) CreateAllLeptons(ctx);
            return Leptons;
        }

        static void CreateAllLeptons(AtomicContext ctx)
        {
            if (Leptons == null)
            {
                Leptons = new List<Contracts.Lepton>();

                foreach (var lepton in ctx.Leptons)
                {
                    Leptons.Add(new Contracts.Lepton()
                    {
                        Symbol = lepton.Symbol,
                        Id = lepton.Id,
                        Charge = lepton.Charge,
                        CommonDecay = lepton.CommonDecay,
                        ElectronicNumber = lepton.ElectronicNumber,
                        Flavor = Enum.GetName(lepton.Flavor.GetType(), lepton.Flavor),
                        Lifetime = lepton.Lifetime,
                        Mass = lepton.Mass,
                        MuonicNumber = lepton.MuonicNumber,
                        Spin = lepton.Spin,
                        TauonicNumber = lepton.TauonicNumber
                    });
                }
            }
        }
    }
}
