﻿using Atomic.Data;
using Atomic.View.Wpf.Data.Interfaces;
using Atomic.View.Wpf.ViewModel;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Atomic.View.Wpf.Data
{
    public class FermionDataService : IFermionDataService
    {
        public async Task<IEnumerable<BosonViewModel>> GetBosonTableList()
        {
            var list = new List<BosonViewModel>();

            using (AtomicContext ctx = new AtomicContext())
            {
                foreach (Boson boson in await ctx.Bosons.Where(b => b.Name != "Higgs boson").ToListAsync())
                {
                    list.Add(new BosonViewModel(new BosonDataService() { Boson = boson }));
                }
            }

            return list;
        }

        public async Task<IEnumerable<LeptonViewModel>> GetLeptonTableList()
        {
            var list = new List<LeptonViewModel>();

            using (AtomicContext ctx = new AtomicContext())
            {
                foreach (Lepton lepton in await ctx.Leptons.ToListAsync())
                {
                    list.Add(new LeptonViewModel(new LeptonDataService() { Lepton = lepton }));
                }
            }

            return list;
        }

        public async Task<IEnumerable<QuarkViewModel>> GetQuarkTableList()
        {
            var list = new List<QuarkViewModel>();

            using (AtomicContext ctx = new AtomicContext())
            {
                foreach (Quark quark in await ctx.Quarks.ToListAsync())
                {
                    list.Add(new QuarkViewModel(new QuarkDataService() { Quark = quark }));
                }
            }

            return list;

        }

        public async Task<BosonViewModel> GetHiggsBoson()
        {
            await Task.Yield();
            var list = new List<BosonViewModel>();

            using (AtomicContext ctx = new AtomicContext())
            {
                var boson = ctx.Bosons.First(b => b.Name == "Higgs boson");
                return new BosonViewModel(new BosonDataService() { Boson = boson });
            }
        }

    }
}
