﻿using Atomic.Data;
using Atomic.View.Wpf.Data.Interfaces;
using System.Linq;

namespace Atomic.View.Wpf.Data.Designer
{
    class DesignBosonDataService : IBosonDataService
    {
        Boson _boson;

        public Boson Boson
        {
            get
            {
                if (_boson == null)
                    _boson = Atomic.Data.Migrations.Configuration.GetSeedBosons().First();

                return _boson;
            }

            set
            {
                _boson = value;
            }
        }

    }
}
