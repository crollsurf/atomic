﻿using Atomic.Data;
using Atomic.View.Wpf.Data.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Atomic.View.Wpf.Data.Designer
{
    class DesignNuclideDataService : INuclideDataService
    {
        Nuclide _nuclide;
        List<Nuclide> _nulcideList;

        public Nuclide Nuclide
        {
            get
            {
                if (_nuclide == null)
                    _nuclide = new Nuclide()
                    {
                        Abundance = 0,
                        ElementId = 89,
                        Energy = 0,
                        HalfLifeSeconds = 0,
                        HalfLifeText = "0",
                        Id = 4053,
                        JPi = "JPi value",
                        Mass = 54.3,
                        NeutronCount = 89,
                        ProtonCount = 148,
                        Symbol = "Ac"
                    };

                return _nuclide;
            }

            set
            {
                _nuclide = value;
            }
        }

        public async Task<IEnumerable<Nuclide>> GetNuclideList()
        {
            await Task.Yield();

            if (_nulcideList == null)
            {
                _nulcideList = new List<Nuclide>();

                //TODO: read from file not database
                using (var client = new AtomicContext())
                {
                    foreach (var item in client.Nuclides.Take(20))
                    {
                        _nulcideList.Add(item);
                    }
                }
            }

            return _nulcideList;

        }
    }
}
