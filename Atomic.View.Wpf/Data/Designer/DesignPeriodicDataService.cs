﻿using Atomic.Data;
using Atomic.View.Wpf.Data.Interfaces;
using Atomic.View.Wpf.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Atomic.View.Wpf.Data.Designer
{
    public class DesignPeriodicDataService : PeriodicDataBase, IPeriodicDataService
    {
        static IEnumerable<Element> _elements;

        public async Task<IEnumerable<IEnumerable<ElementViewModel>>> GetPeriodicTableList()
        {
            await Task.Yield();
            return GetPeriodicTableListFromElements(GetElements());
        }

        public async Task<IEnumerable<IEnumerable<ElementViewModel>>> GetSecondTableList()
        {
            await Task.Yield();
            return GetSecondTableListFromElements(GetElements());
        }

        public IEnumerable<Element> GetElements()
        {
            if (_elements == null)
                _elements = Atomic.Data.Migrations.Configuration.GetSeedElements();

            return _elements;
        }


    }
}