﻿using Atomic.Data;
using Atomic.View.Wpf.Data.Interfaces;
using System.Linq;

namespace Atomic.View.Wpf.Data.Designer
{
    class DesignElementDataService : IElementDataService
    {
        Element _element;

        public Element Element
        {
            get
            {
                if (_element == null)
                    _element = Atomic.Data.Migrations.Configuration.GetSeedElements().First();

                return _element;
            }

            set
            {
                _element = value;
            }
        }

     }
}
