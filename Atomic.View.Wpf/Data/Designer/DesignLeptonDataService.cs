﻿using Atomic.Data;
using Atomic.View.Wpf.Data.Interfaces;
using System.Linq;

namespace Atomic.View.Wpf.Data.Designer
{
    class DesignLeptonDataService : ILeptonDataService
    {
        Lepton _lepton;

        public Lepton Lepton
        {
            get
            {
                if (_lepton == null)
                    _lepton = Atomic.Data.Migrations.Configuration.GetSeedLeptons().First();

                return _lepton;
            }

            set
            {
                _lepton = value;
            }
        }

    }
}
