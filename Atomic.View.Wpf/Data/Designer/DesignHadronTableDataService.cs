﻿using Atomic.Data;
using Atomic.View.Wpf.Data.Designer;
using Atomic.View.Wpf.Data.Interfaces;
using Atomic.View.Wpf.ViewModel;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Atomic.View.Wpf.Data.Designer
{
    public class DesignHadronTableDataService : IHadronTableDataService
    {
        public async Task<IEnumerable<HadronViewModel>> GetBaryonList()
        {
            var list = new List<HadronViewModel>();

            using (AtomicContext ctx = new AtomicContext())
            {
                foreach (Hadron hadron in await ctx.Hadrons.Where(h => h.Family == HadronFamily.Baryon).ToListAsync())
                {
                    list.Add(new HadronViewModel(new DesignHadronDataService() { Hadron = hadron }));
                }
            }

            return list;
        }

        public async Task<IEnumerable<HadronViewModel>> GetMesonList()
        {
            var list = new List<HadronViewModel>();

            using (AtomicContext ctx = new AtomicContext())
            {
                foreach (Hadron hadron in await ctx.Hadrons.Where(h => h.Family == HadronFamily.Meson).ToListAsync())
                {
                    list.Add(new HadronViewModel(new DesignHadronDataService() { Hadron = hadron }));
                }
            }

            return list;
        }


    }
}
