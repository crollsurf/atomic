﻿using Atomic.Data;
using Atomic.View.Wpf.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Atomic.View.Wpf.Data.Designer
{
    class DesignHadronDataService : IHadronDataService
    {
        Hadron _hadron;

        public Hadron Hadron
        {
            get
            {
                if (_hadron == null)
                {
                    var up = Atomic.Data.Migrations.Configuration.GetSeedQuarks().Where(q => q.Flavor == QuarkFlavor.Up).First();
                    var down = Atomic.Data.Migrations.Configuration.GetSeedQuarks().Where(q => q.Flavor == QuarkFlavor.Down).First();
                    _hadron = Atomic.Data.Migrations.Configuration.CreateHadron(HadronFamily.Baryon, "Proton[7] with extra words", "p+", new List<Quark>() { up, up, down }, 938.272046, 0.5, 0.5, 1.0, double.MinValue, 0, 0, 0, "Stable[b]", "Unobserved");
                    }

                return _hadron;
            }

            set
            {
                _hadron = value;
            }
        }

    }
}
