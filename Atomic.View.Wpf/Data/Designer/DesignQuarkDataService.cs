﻿using Atomic.Data;
using Atomic.View.Wpf.Data.Interfaces;
using System.Linq;

namespace Atomic.View.Wpf.Data.Designer
{
    class DesignQuarkDataService : IQuarkDataService
    {
        Quark _quark;

        public Quark Quark
        {
            get
            {
                if (_quark == null)
                    _quark = Atomic.Data.Migrations.Configuration.GetSeedQuarks().First();

                return _quark;
            }

            set
            {
                _quark = value;
            }
        }

    }
}
