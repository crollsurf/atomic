﻿using Atomic.View.Wpf.Data.Interfaces;
using Atomic.View.Wpf.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Atomic.View.Wpf.Data.Designer
{
    public class DesignFermionDataService : IFermionDataService
    {
        public async Task<IEnumerable<BosonViewModel>> GetBosonTableList()
        {
            await Task.Yield();
            var list = new List<BosonViewModel>();

            foreach (var boson in Atomic.Data.Migrations.Configuration.GetSeedBosons().Where(b => b.Name != "Higgs boson"))
            {
                list.Add( new BosonViewModel(new DesignBosonDataService() { Boson = boson }));
            }

            return list;
        }

        public async Task<IEnumerable<LeptonViewModel>> GetLeptonTableList()
        {
            await Task.Yield();
            var list = new List<LeptonViewModel>();

            foreach (var lepton in Atomic.Data.Migrations.Configuration.GetSeedLeptons())
            {
                list.Add(new LeptonViewModel(new DesignLeptonDataService() { Lepton = lepton }));
            }

            return list;
        }

        public async Task<IEnumerable<QuarkViewModel>> GetQuarkTableList()
        {
            await Task.Yield();
            var list = new List<QuarkViewModel>();

            foreach (var quark in Atomic.Data.Migrations.Configuration.GetSeedQuarks())
            {
                list.Add(new QuarkViewModel(new DesignQuarkDataService() { Quark = quark }));
            }

            return list;
        }

        public async Task<BosonViewModel> GetHiggsBoson()
        {
            await Task.Yield();
            var list = new List<BosonViewModel>();

            var boson = Atomic.Data.Migrations.Configuration.GetSeedBosons().First(b => b.Name == "Higgs boson");
            return new BosonViewModel(new BosonDataService() { Boson = boson });
        }
    }
}
