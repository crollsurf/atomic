﻿using Atomic.Data;
using Atomic.View.Wpf.Data.Interfaces;

namespace Atomic.View.Wpf.Data
{
    public class LeptonDataService : ILeptonDataService
    {
        public Lepton Lepton { get; set; }
    }
}
