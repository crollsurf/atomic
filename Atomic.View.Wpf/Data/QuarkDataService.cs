﻿using Atomic.Data;
using Atomic.View.Wpf.Data.Interfaces;

namespace Atomic.View.Wpf.Data
{
    public class QuarkDataService : IQuarkDataService
    {
        public Quark Quark { get; set; }
    }
}
