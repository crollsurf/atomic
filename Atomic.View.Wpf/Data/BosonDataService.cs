﻿using Atomic.Data;
using Atomic.View.Wpf.Data.Interfaces;

namespace Atomic.View.Wpf.Data
{
    public class BosonDataService : IBosonDataService
    {
        public Boson Boson { get; set; }
    }
}
