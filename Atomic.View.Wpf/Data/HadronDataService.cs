﻿using Atomic.Data;
using Atomic.View.Wpf.Data.Interfaces;

namespace Atomic.View.Wpf.Data
{
    public class HadronDataService : IHadronDataService
    {
        public Hadron Hadron { get; set; }
    }
}
