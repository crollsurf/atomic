﻿using Atomic.Data;
using Atomic.View.Wpf.Data.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Atomic.View.Wpf.Data
{
    public class NuclideDataService : INuclideDataService
    {
        List<Nuclide> _nulcideList;

        public Nuclide Nuclide { get; set; }

        public async Task<IEnumerable<Nuclide>> GetNuclideList()
        {
            if (_nulcideList == null)
            {
                using (var client = new AtomicContext())
                {
                    _nulcideList = await client.Nuclides.Include("Decay").ToListAsync();
                }
            }

            return _nulcideList;
        }
    }
}
