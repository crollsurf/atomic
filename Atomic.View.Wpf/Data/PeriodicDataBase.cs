﻿using Atomic.Data;
using Atomic.View.Wpf.ViewModel;
using System.Collections.Generic;
using System.Linq;

namespace Atomic.View.Wpf.Data
{
    public class PeriodicDataBase
    {
        public IEnumerable<IEnumerable<ElementViewModel>> GetPeriodicTableListFromElements(IEnumerable<Element> elements)
        {
            List<List<ElementViewModel>> periodicTableList = new List<List<ElementViewModel>>();

            for (int i = 0; i < 7; i++)
            {
                int period = i + 1;
                var subList = new List<ElementViewModel>(18);

                for (int z = 0; z < 18; z++)
                    subList.Add(null);

                periodicTableList.Add(subList);

                for (int j = 0; j < 18; j++)
                {
                    int group = j + 1;

                    var element = elements.FirstOrDefault(e => e.Group == group && e.Period == period);
                    if (element != null)
                    {
                        var data = new ElementDataService() { Element = element };
                        subList[j] = new ElementViewModel(data);
                    }
                }
            }

            return periodicTableList;
        }

        public IEnumerable<IEnumerable<ElementViewModel>> GetSecondTableListFromElements(IEnumerable<Element> elements)
        {
            List<List<ElementViewModel>> secondTable = new List<List<ElementViewModel>>();
            secondTable.Add(GetLanthanideTableList(elements));
            secondTable.Add(GetActnideTableList(elements));

            return secondTable;
        }

        List<ElementViewModel> GetLanthanideTableList(IEnumerable<Element> elements)
        {
            return elements.Where(e => e.Number >= 57 && e.Number <= 71)
                .AsEnumerable().Select(e => new ElementViewModel(new ElementDataService { Element = e }))
                .ToList();
        }

        List<ElementViewModel> GetActnideTableList(IEnumerable<Element> elements)
        {
            return elements.Where(e => e.Number >= 89 && e.Number <= 103)
                .AsEnumerable().Select(e => new ElementViewModel(new ElementDataService { Element = e }))
                .ToList();
        }
    }

}

