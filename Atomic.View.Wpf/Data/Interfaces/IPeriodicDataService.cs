﻿using Atomic.View.Wpf.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Atomic.View.Wpf.Data.Interfaces
{
    public interface IPeriodicDataService
    {
        Task<IEnumerable<IEnumerable<ElementViewModel>>> GetPeriodicTableList();
        Task<IEnumerable<IEnumerable<ElementViewModel>>> GetSecondTableList();
    }
}
