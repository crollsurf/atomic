﻿using Atomic.Data;

namespace Atomic.View.Wpf.Data.Interfaces
{
    public interface IBosonDataService
    {
        Boson Boson { get; set; }
    }
}
