﻿using Atomic.View.Wpf.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Atomic.View.Wpf.Data.Interfaces
{
    public interface IFermionDataService
    {
        Task<IEnumerable<QuarkViewModel>> GetQuarkTableList();
        Task<IEnumerable<LeptonViewModel>> GetLeptonTableList();
        Task<IEnumerable<BosonViewModel>> GetBosonTableList();
        Task<BosonViewModel> GetHiggsBoson();
    }
}
