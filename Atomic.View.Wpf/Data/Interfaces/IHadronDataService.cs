﻿using Atomic.Data;

namespace Atomic.View.Wpf.Data.Interfaces
{
    public interface IHadronDataService
    {
        Hadron Hadron { get; set; }
    }
}
