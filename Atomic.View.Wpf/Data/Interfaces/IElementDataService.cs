﻿using Atomic.Data;

namespace Atomic.View.Wpf.Data.Interfaces
{
    public interface IElementDataService
    {
        Element Element { get; set; }
    }
}
