﻿using Atomic.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Atomic.View.Wpf.Data.Interfaces
{
    public interface INuclideDataService
    {
        Nuclide Nuclide { get; }
        Task<IEnumerable<Nuclide>> GetNuclideList();
    }
}
