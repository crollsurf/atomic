﻿using Atomic.Data;

namespace Atomic.View.Wpf.Data.Interfaces
{
    public interface IQuarkDataService
    {
        Quark Quark { get; set; }
    }
}
