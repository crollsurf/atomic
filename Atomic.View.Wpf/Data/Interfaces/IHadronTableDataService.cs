﻿using Atomic.Data;
using Atomic.View.Wpf.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Atomic.View.Wpf.Data.Interfaces
{
    public interface IHadronTableDataService
    {
        Task<IEnumerable<HadronViewModel>> GetBaryonList();
        Task<IEnumerable<HadronViewModel>> GetMesonList();
    }
}
