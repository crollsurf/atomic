﻿using Atomic.Data;

namespace Atomic.View.Wpf.Data.Interfaces
{
    public interface ILeptonDataService
    {
        Lepton Lepton { get; set; }
    }
}
