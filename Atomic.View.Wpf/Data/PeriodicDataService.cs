﻿using Atomic.Data;
using Atomic.View.Wpf.Data.Interfaces;
using Atomic.View.Wpf.ViewModel;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Atomic.View.Wpf.Data
{
    public class PeriodicDataService : PeriodicDataBase, IPeriodicDataService
    {
        public async Task<IEnumerable<IEnumerable<ElementViewModel>>> GetPeriodicTableList()
        {
            using (var client = new AtomicContext())
            {
                var list = await client.Elements.ToListAsync();
                return GetPeriodicTableListFromElements(list);
            }
        }

        public async Task<IEnumerable<IEnumerable<ElementViewModel>>> GetSecondTableList()
        {
            using (var client = new AtomicContext())
            {
                var list = await client.Elements.ToListAsync();
                return GetSecondTableListFromElements(list);
            }
        }

    }
}
