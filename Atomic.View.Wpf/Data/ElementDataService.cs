﻿using Atomic.Data;
using Atomic.View.Wpf.Data.Interfaces;

namespace Atomic.View.Wpf.Data
{
    public class ElementDataService : IElementDataService
    {
        public Element Element { get; set; }
    }
}
