﻿using System.Windows.Controls;

namespace Atomic.View.Wpf.View.Hadrons
{
    /// <summary>
    /// Description for MesonTableView.
    /// </summary>
    public partial class MesonTableView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the MesonTableView class.
        /// </summary>
        public MesonTableView()
        {
            InitializeComponent();
        }
    }
}