﻿using System.Windows.Controls;

namespace Atomic.View.Wpf.View.Hadrons
{
    /// <summary>
    /// Description for HadronTableView.
    /// </summary>
    public partial class HadronTableView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the HadronTableView class.
        /// </summary>
        public HadronTableView()
        {
            InitializeComponent();
        }
    }
}