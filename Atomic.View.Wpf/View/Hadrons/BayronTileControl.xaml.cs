﻿using System.Windows;
using System.Windows.Controls;

namespace Atomic.View.Wpf.View.Hadrons
{
    /// <summary>
    /// Description for BayronTileControl.
    /// </summary>
    public partial class BayronTileControl : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the BayronTileControl class.
        /// </summary>
        public BayronTileControl()
        {
            InitializeComponent();
        }
    }
}