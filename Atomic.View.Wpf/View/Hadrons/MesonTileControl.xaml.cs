﻿using System.Windows.Controls;

namespace Atomic.View.Wpf.View.Hadrons
{
    /// <summary>
    /// Description for MesonTileControl.
    /// </summary>
    public partial class MesonTileControl : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the MesonTileControl class.
        /// </summary>
        public MesonTileControl()
        {
            InitializeComponent();
        }
    }
}