﻿using Atomic.View.Wpf.ViewModel;
using MahApps.Metro.Controls;

namespace Atomic.View.Wpf.View.Hadrons
{
    /// <summary>
    /// Description for HadronView.
    /// </summary>
    public partial class HadronView : MetroWindow
    {
        /// <summary>
        /// Initializes a new instance of the HadronView class.
        /// </summary>
        public HadronView()
        {
            InitializeComponent();
        }

        public HadronView(HadronViewModel model)
        {
            DataContext = model;
            InitializeComponent();
        }

    }
}