﻿using Atomic.View.Wpf.ViewModel;
using MahApps.Metro.Controls;

namespace Atomic.View.Wpf.View.Elements
{
    public partial class ElementView : MetroWindow
    {
        public ElementView()
        {
            InitializeComponent();
        }

        public ElementView(ElementViewModel model)
        {
            DataContext = model;
            InitializeComponent();
        }
    }
}
