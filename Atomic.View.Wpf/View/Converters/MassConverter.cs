﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Atomic.View.Wpf.View.Converters
{
    [ValueConversion(typeof(double), typeof(String))]
    public class MassConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double number = (double)value;
            return (number == 0) ? "0" : "~" + number.ToString("F") + " Mev/c\xB2";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.ToString();
        }
    }
}
