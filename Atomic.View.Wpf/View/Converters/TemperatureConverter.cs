﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Atomic.View.Wpf.View.Converters
{
    [ValueConversion(typeof(double), typeof(String))]
    public class TemperatureConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return "";

            double degrees = (double)value;
            return string.Format("{0}°C", degrees);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.ToString();
        }
    }
}
