﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Atomic.View.Wpf.View.Fermions
{
    /// <summary>
    /// Interaction logic for FermionTableView.xaml
    /// </summary>
    public partial class FermionTableView : UserControl
    {
        public FermionTableView()
        {
            InitializeComponent();
        }
    }
}
