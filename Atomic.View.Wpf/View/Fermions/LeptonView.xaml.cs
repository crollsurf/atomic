﻿using Atomic.View.Wpf.ViewModel;
using MahApps.Metro.Controls;
using System.Windows;

namespace Atomic.View.Wpf.View.Fermions
{
    public partial class LeptonView : MetroWindow
    {
        public LeptonView()
        {
            InitializeComponent();
        }

        public LeptonView(LeptonViewModel model)
        {
            DataContext = model;
            InitializeComponent();
        }

    }
}