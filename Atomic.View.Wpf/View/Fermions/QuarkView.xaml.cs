﻿using Atomic.View.Wpf.ViewModel;
using MahApps.Metro.Controls;

namespace Atomic.View.Wpf.View.Fermions
{
    /// <summary>
    /// Description for QuarkView.
    /// </summary>
    public partial class QuarkView : MetroWindow
    {
        /// <summary>
        /// Initializes a new instance of the QuarkView class.
        /// </summary>
        public QuarkView()
        {
            InitializeComponent();
        }

        public QuarkView(QuarkViewModel model)
        {
            DataContext = model;
            InitializeComponent();
        }
    }
}