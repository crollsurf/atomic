﻿using Atomic.View.Wpf.ViewModel;
using MahApps.Metro.Controls;

namespace Atomic.View.Wpf.View.Fermions
{
    public partial class BosonView : MetroWindow
    {
        public BosonView()
        {
            InitializeComponent();
        }

        public BosonView(BosonViewModel model)
        {
            DataContext = model;
            InitializeComponent();
        }

    }
}