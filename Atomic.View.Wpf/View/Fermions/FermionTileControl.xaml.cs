﻿using System.Windows.Controls;

namespace Atomic.View.Wpf.View.Fermions
{
    /// <summary>
    /// Interaction logic for FermionTileControl.xaml
    /// </summary>
    public partial class FermionTileControl : UserControl
    {
        public FermionTileControl()
        {
            InitializeComponent();
        }
    }
}
