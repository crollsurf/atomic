﻿using System.Windows.Controls;

namespace Atomic.View.Wpf.View.Nuclides
{
    /// <summary>
    /// Description for NuclideDataView.
    /// </summary>
    public partial class NuclideDataView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the NuclideDataView class.
        /// </summary>
        public NuclideDataView()
        {
            InitializeComponent();
        }
    }
}