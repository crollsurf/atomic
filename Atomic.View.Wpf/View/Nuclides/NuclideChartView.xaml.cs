﻿using System.Windows.Controls;

namespace Atomic.View.Wpf.View.Nuclides
{
    /// <summary>
    /// Interaction logic for NuclideChartView.xaml
    /// </summary>
    public partial class NuclideChartView : UserControl
    {
        public NuclideChartView()
        {
            InitializeComponent();
        }
    }
}
