﻿using System.Windows;
using System.Windows.Controls;

namespace Atomic.View.Wpf.View.Nuclides
{
    /// <summary>
    /// Description for NuclideTileView.
    /// </summary>
    public partial class NuclideTileControl : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the NuclideTileView class.
        /// </summary>
        public NuclideTileControl()
        {
            InitializeComponent();
        }
    }
}