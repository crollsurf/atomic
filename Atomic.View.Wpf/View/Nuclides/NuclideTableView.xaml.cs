﻿using System.Windows.Controls;

namespace Atomic.View.Wpf.View.Nuclides
{
    /// <summary>
    /// Description for NuclideTableView.
    /// </summary>
    public partial class NuclideTableView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the NuclideTableView class.
        /// </summary>
        public NuclideTableView()
        {
            InitializeComponent();
        }
    }
}