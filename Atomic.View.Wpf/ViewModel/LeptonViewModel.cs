﻿using Atomic.View.Wpf.Data.Interfaces;
using Atomic.View.Wpf.View.Fermions;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls;
using System.Windows;
using System.Windows.Media;

namespace Atomic.View.Wpf.ViewModel
{
    public class LeptonViewModel : ViewModelBase, IFermionViewModel
    {
        ILeptonDataService _data;
        static RadialGradientBrush _fermionColor;

        public LeptonViewModel(ILeptonDataService data)
        {
            _data = data;
            this.CloseWindowCommand = new RelayCommand<MetroWindow>(CloseWindow);
            this.DisplayDetailCommand = new RelayCommand(this.DisplayDetail, CanDisplayDetail);
        }

        #region Commands

        public RelayCommand DisplayDetailCommand { get; private set; }

        public bool CanDisplayDetail()
        {
            return _data.Lepton != null;
        }

        public void DisplayDetail()
        {
            var detail = new LeptonView(this);
            detail.Owner = Application.Current.MainWindow;
            detail.Show();
        }

        public RelayCommand<MetroWindow> CloseWindowCommand { get; private set; }

        private void CloseWindow(MetroWindow window)
        {
            window.Close();
        }

        #endregion Commands

        #region Properties

        public double Charge
        {
            get { return _data.Lepton.Charge; }
        }

        public string CommonDecay
        {
            get { return _data.Lepton.CommonDecay; }
        }

        public int ElectronicNumber
        {
            get { return _data.Lepton.ElectronicNumber; }
        }

        public string Name
        {
            get { return _data.Lepton.Flavor.ToString(); }
        }

        public string Lifetime
        {
            get { return _data.Lepton.Lifetime; }
        }

        public double Mass
        {
            get { return _data.Lepton.Mass; }
        }

        public int MuonicNumber
        {
            get { return _data.Lepton.MuonicNumber; }
        }

        public double Spin
        {
            get { return _data.Lepton.Spin; }
        }

        public string Symbol
        {
            get { return _data.Lepton.Symbol; }
        }

        public int TauonicNumber
        {
            get { return _data.Lepton.TauonicNumber; }
        }

        #endregion Properties

        #region DisplayProperties

        public RadialGradientBrush FermionColour
        {
            get
            {
                if (_fermionColor == null)
                {
                    _fermionColor = new RadialGradientBrush();
                    _fermionColor.GradientOrigin = new Point(0.0, 0.0);
                    _fermionColor.GradientStops.Add(new GradientStop(Colors.Lime, 0.0));
                    _fermionColor.GradientStops.Add(new GradientStop(Colors.LimeGreen, 1.0));
                }

                return _fermionColor;
            }
        }

        #endregion DisplyProperties
    }
}