﻿using Atomic.View.Wpf.Data.Interfaces;
using Atomic.View.Wpf.View.Fermions;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls;
using System.Windows;
using System.Windows.Media;

namespace Atomic.View.Wpf.ViewModel
{
    public class BosonViewModel : ViewModelBase, IFermionViewModel
    {
        IBosonDataService _data;
        static RadialGradientBrush _fermionColor;
        static RadialGradientBrush _higgsBosonColor;
        bool _ishiggsBoson;

        public BosonViewModel(IBosonDataService data)
        {
            _data = data;
            _ishiggsBoson = data.Boson.Name == "Higgs boson";

            this.CloseWindowCommand = new RelayCommand<MetroWindow>(CloseWindow);
            this.DisplayDetailCommand = new RelayCommand(this.DisplayDetail, CanDisplayDetail);
        }

        #region Commands

        public RelayCommand DisplayDetailCommand { get; private set; }

        public bool CanDisplayDetail()
        {
            return _data.Boson != null;
        }

        public void DisplayDetail()
        {
            var detail = new BosonView(this);
            detail.Owner = Application.Current.MainWindow;
            detail.Show();
        }

        public RelayCommand<MetroWindow> CloseWindowCommand { get; private set; }

        private void CloseWindow(MetroWindow window)
        {
            window.Close();
        }

        #endregion Commands

        #region Properties

        public string Anitparticle
        {
            get { return _data.Boson.Anitparticle; }
        }

        public double Charge
        {
            get { return _data.Boson.Charge; }
        }

        public string InteractionMediated
        {
            get { return _data.Boson.InteractionMediated; }
        }

        public double Mass
        {
            get { return _data.Boson.Mass; }
        }

        public string Name
        {
            get { return _data.Boson.Name; }
        }

        public double Spin
        {
            get { return _data.Boson.Spin; }
        }

        public string Symbol
        {
            get { return _data.Boson.Symbol; }
        }

        #endregion Properties

        #region DisplayProperties

        public RadialGradientBrush FermionColour
        {
            get
            {
                if (_higgsBosonColor == null)
                {
                    _higgsBosonColor = new RadialGradientBrush();
                    _higgsBosonColor.GradientOrigin = new Point(0.0, 0.0);
                    _higgsBosonColor.GradientStops.Add(new GradientStop(Colors.Yellow, 0.0));
                    _higgsBosonColor.GradientStops.Add(new GradientStop(Colors.Gold, 1.0));
                }

                if (_fermionColor == null)
                {
                    _fermionColor = new RadialGradientBrush();
                    _fermionColor.GradientOrigin = new Point(0.0, 0.0);
                    _fermionColor.GradientStops.Add(new GradientStop(Colors.LightSalmon, 0.0));
                    _fermionColor.GradientStops.Add(new GradientStop(Colors.Tomato, 1.0));
                }

                return _ishiggsBoson ? _higgsBosonColor : _fermionColor;
            }
        }

        public SolidColorBrush LightFermionColour
        {
            get
            {
                string hex = _ishiggsBoson ? "#fff1b3" : "#ffe9e6";
                return new SolidColorBrush((Color)ColorConverter.ConvertFromString(hex));
            }
        }

        #endregion DisplyProperties

    }
}