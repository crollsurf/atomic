﻿using Atomic.Data;
using Atomic.View.Wpf.Data;
using Atomic.View.Wpf.Data.Interfaces;
using GalaSoft.MvvmLight;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Atomic.View.Wpf.ViewModel
{
    public class NuclideDataViewModel : ViewModelBase
    {
        public enum DecayType { Stable, Alpha, Beta, NeutronEmission, ProtonEmission, SpontaniousEmission, PositronEmission, ElectronCapture };

        INuclideDataService _data;
        EnumerableDataSource<NuclideViewModel> _stableChart;
        EnumerableDataSource<NuclideViewModel> _alphaChart;
        EnumerableDataSource<NuclideViewModel> _betaChart;
        EnumerableDataSource<NuclideViewModel> _neutronEmissionChart;
        EnumerableDataSource<NuclideViewModel> _protonEmissionChart;
        EnumerableDataSource<NuclideViewModel> _spontaniousEmissionChart;
        EnumerableDataSource<NuclideViewModel> _positronEmissionChart;
        EnumerableDataSource<NuclideViewModel> _electronCaptureChart;

        public NuclideDataViewModel(INuclideDataService data)
        {
            _data = data;

            NuclideTableData = new NotifyTaskCompletion<IEnumerable<IEnumerable<NuclideTileViewModel>>>(GetNuclideTableListFromNuclides());
            NuclideData = new NotifyTaskCompletion<IEnumerable<Nuclide>>(_data.GetNuclideList());
            NuclideStableChartData = new NotifyTaskCompletion<EnumerableDataSource<NuclideViewModel>>(GetNuclideStableChartData());
            NuclideAlphaChartData = new NotifyTaskCompletion<EnumerableDataSource<NuclideViewModel>>(GetNuclideAlphaChartData());
            NuclideBetaChartData = new NotifyTaskCompletion<EnumerableDataSource<NuclideViewModel>>(GetNuclideBetaChartData());
            NuclideNeutronEmissionChartData = new NotifyTaskCompletion<EnumerableDataSource<NuclideViewModel>>(GetNuclideNeutronEmissionChartData());
            NuclideProtonEmissionChartData = new NotifyTaskCompletion<EnumerableDataSource<NuclideViewModel>>(GetNuclideProtonEmissionChartData());
            NuclideSpontaniousEmissionChartData = new NotifyTaskCompletion<EnumerableDataSource<NuclideViewModel>>(GetNuclideSpontaniousEmissionChartData());
            NuclidePositronEmissionChartData = new NotifyTaskCompletion<EnumerableDataSource<NuclideViewModel>>(GetNuclidePositronEmissionChartData());
            NuclideElectronCaptureChartData = new NotifyTaskCompletion<EnumerableDataSource<NuclideViewModel>>(GetNuclideElectronCaptureChartData());
        }

        public NotifyTaskCompletion<IEnumerable<IEnumerable<NuclideTileViewModel>>> NuclideTableData { get; private set; }

        public NotifyTaskCompletion<IEnumerable<Nuclide>> NuclideData { get; private set; }

        public NotifyTaskCompletion<EnumerableDataSource<NuclideViewModel>> NuclideStableChartData { get; private set; }

        public NotifyTaskCompletion<EnumerableDataSource<NuclideViewModel>> NuclideAlphaChartData { get; private set; }

        public NotifyTaskCompletion<EnumerableDataSource<NuclideViewModel>> NuclideBetaChartData { get; private set; }

        public NotifyTaskCompletion<EnumerableDataSource<NuclideViewModel>> NuclideNeutronEmissionChartData { get; private set; }

        public NotifyTaskCompletion<EnumerableDataSource<NuclideViewModel>> NuclideProtonEmissionChartData { get; private set; }

        public NotifyTaskCompletion<EnumerableDataSource<NuclideViewModel>> NuclideSpontaniousEmissionChartData { get; private set; }

        public NotifyTaskCompletion<EnumerableDataSource<NuclideViewModel>> NuclidePositronEmissionChartData { get; private set; }

        public NotifyTaskCompletion<EnumerableDataSource<NuclideViewModel>> NuclideElectronCaptureChartData { get; private set; }

        async Task<IEnumerable<IEnumerable<NuclideTileViewModel>>> GetNuclideTableListFromNuclides()
        {
            int maxProtonCount = 117;
            List<List<NuclideTileViewModel>> nuclideTableList = new List<List<NuclideTileViewModel>>();

            var list = await _data.GetNuclideList();

            for (int z = maxProtonCount; z > 0; z--)
            {
                var elementNuclides = list.Where(n => n.ProtonCount == z && n.ElementId.HasValue).OrderBy(n => n.NeutronCount).ToList();

                var subList = new List<NuclideTileViewModel>();
                subList.AddRange(elementNuclides.Select(n => new NuclideTileViewModel(new NuclideDataService() { Nuclide = n})).ToList());
                nuclideTableList.Add(subList);
            }

            return nuclideTableList;
        }

        async Task<EnumerableDataSource<NuclideViewModel>> GetNuclideStableChartData()
        {
            await GetNuclideDataPoints();
            return _stableChart;
        }

        async Task<EnumerableDataSource<NuclideViewModel>> GetNuclideAlphaChartData()
        {
            await GetNuclideDataPoints();
            return _alphaChart;
        }

        async Task<EnumerableDataSource<NuclideViewModel>> GetNuclideBetaChartData()
        {
            await GetNuclideDataPoints();
            return _betaChart;
        }

        async Task<EnumerableDataSource<NuclideViewModel>> GetNuclideNeutronEmissionChartData()
        {
            await GetNuclideDataPoints();
            return _neutronEmissionChart;
        }

        async Task<EnumerableDataSource<NuclideViewModel>> GetNuclideProtonEmissionChartData()
        {
            await GetNuclideDataPoints();
            return _protonEmissionChart;
        }

        async Task<EnumerableDataSource<NuclideViewModel>> GetNuclideSpontaniousEmissionChartData()
        {
            await GetNuclideDataPoints();
            return _spontaniousEmissionChart;
        }

        async Task<EnumerableDataSource<NuclideViewModel>> GetNuclidePositronEmissionChartData()
        {
            await GetNuclideDataPoints();
            return _positronEmissionChart;
        }

        async Task<EnumerableDataSource<NuclideViewModel>> GetNuclideElectronCaptureChartData()
        {
            await GetNuclideDataPoints();
            return _electronCaptureChart;
        }

        public async Task<EnumerableDataSource<NuclideViewModel>> GetNuclideDataPoints()
        {
            if (_stableChart == null)
            {
                var stable = new List<NuclideViewModel>();
                var alpha = new List<NuclideViewModel>();
                var beta = new List<NuclideViewModel>();
                var neutronEmissions = new List<NuclideViewModel>();
                var protonEmissions = new List<NuclideViewModel>();
                var spontaniousEmissions = new List<NuclideViewModel>();
                var positronEmissions = new List<NuclideViewModel>();
                var electronCaptures = new List<NuclideViewModel>();
                var list = await _data.GetNuclideList();

                foreach (var item in list)
                {
                    if (double.MaxValue == item.HalfLifeSeconds)
                    {
                        stable.Add(new NuclideViewModel(item));
                    }
                    else
                    {
                        NuclideViewModel.DecayType decay = GetNuclideDecay(item);

                        switch (decay)
                        {
                            case NuclideViewModel.DecayType.Stable:
                                stable.Add(new NuclideViewModel(item));
                                break;
                            case NuclideViewModel.DecayType.Alpha:
                                alpha.Add(new NuclideViewModel(item));
                                break;
                            case NuclideViewModel.DecayType.Beta:
                                beta.Add(new NuclideViewModel(item));
                                break;
                            case NuclideViewModel.DecayType.NeutronEmission:
                                neutronEmissions.Add(new NuclideViewModel(item));
                                break;
                            case NuclideViewModel.DecayType.ProtonEmission:
                                protonEmissions.Add(new NuclideViewModel(item));
                                break;
                            case NuclideViewModel.DecayType.SpontaniousEmission:
                                spontaniousEmissions.Add(new NuclideViewModel(item));
                                break;
                            case NuclideViewModel.DecayType.PositronEmission:
                                positronEmissions.Add(new NuclideViewModel(item));
                                break;
                            case NuclideViewModel.DecayType.ElectronCapture:
                                electronCaptures.Add(new NuclideViewModel(item));
                                break;
                            default:
                                break;
                        }

                    }
                }

                _stableChart = CreateNewDataSource(stable);
                _alphaChart = CreateNewDataSource(alpha);
                _betaChart = CreateNewDataSource(beta);
                _neutronEmissionChart = CreateNewDataSource(neutronEmissions);
                _protonEmissionChart = CreateNewDataSource(protonEmissions);
                _spontaniousEmissionChart = CreateNewDataSource(spontaniousEmissions);
                _positronEmissionChart = CreateNewDataSource(positronEmissions);
                _electronCaptureChart = CreateNewDataSource(electronCaptures);

            }

            return _stableChart;
        }

        private EnumerableDataSource<NuclideViewModel> CreateNewDataSource(List<NuclideViewModel> list)
        {
            var source = new EnumerableDataSource<NuclideViewModel>(list);
            source.SetXMapping(p => p.NeutronCount);
            source.SetYMapping(p => p.ProtonCount);

            return source;
        }

        private NuclideViewModel.DecayType GetNuclideDecay(Nuclide item)
        {
            if (item.Decay == null || item.Decay.Count == 0)
            {
                return NuclideViewModel.DecayType.Stable;
            }
            else
            {
                RadiationDecayType decay = item.Decay[0].DecayMode;

                switch (decay)
                {
                    case RadiationDecayType.IT:
                        return NuclideViewModel.DecayType.Alpha;
                    case RadiationDecayType.BMinus:
                        return NuclideViewModel.DecayType.Beta;
                    case RadiationDecayType.TwoBMinus:
                        return NuclideViewModel.DecayType.Beta;
                    case RadiationDecayType.EC:
                        return NuclideViewModel.DecayType.ElectronCapture;
                    case RadiationDecayType.EA:
                        return NuclideViewModel.DecayType.ElectronCapture;
                    case RadiationDecayType.EP:
                        return NuclideViewModel.DecayType.ElectronCapture;
                    case RadiationDecayType.E2P:
                        return NuclideViewModel.DecayType.ElectronCapture;
                    case RadiationDecayType.TwoEC:
                        return NuclideViewModel.DecayType.ElectronCapture;
                    case RadiationDecayType.E3P:
                        return NuclideViewModel.DecayType.ElectronCapture;
                    case RadiationDecayType.N:
                        return NuclideViewModel.DecayType.NeutronEmission;
                    case RadiationDecayType.TwoN:
                        return NuclideViewModel.DecayType.NeutronEmission;
                    case RadiationDecayType.TwoNQuestionMark:
                        return NuclideViewModel.DecayType.NeutronEmission;
                    case RadiationDecayType.BN:
                        return NuclideViewModel.DecayType.Beta;
                    case RadiationDecayType.B2N:
                        return NuclideViewModel.DecayType.Beta;
                    case RadiationDecayType.BNA:
                        return NuclideViewModel.DecayType.Beta;
                    case RadiationDecayType.B3N:
                        return NuclideViewModel.DecayType.Beta;
                    case RadiationDecayType.B4N:
                        return NuclideViewModel.DecayType.Beta;
                    case RadiationDecayType.P:
                        return NuclideViewModel.DecayType.ProtonEmission;
                    case RadiationDecayType.TwoP:
                        return NuclideViewModel.DecayType.ProtonEmission;
                    case RadiationDecayType.A:
                        return NuclideViewModel.DecayType.Alpha;
                    case RadiationDecayType.BA:
                        return NuclideViewModel.DecayType.Beta;
                    case RadiationDecayType.TwoA:
                        return NuclideViewModel.DecayType.Alpha;
                    case RadiationDecayType.B3A:
                        return NuclideViewModel.DecayType.Alpha;
                    case RadiationDecayType.TwelveC:
                        return NuclideViewModel.DecayType.Beta;
                    case RadiationDecayType.FourteenC:
                        return NuclideViewModel.DecayType.Beta;
                    case RadiationDecayType.TwentyO:
                        return NuclideViewModel.DecayType.Beta;
                    case RadiationDecayType.TwentyFourNe:
                        return NuclideViewModel.DecayType.NeutronEmission;
                    case RadiationDecayType.TwentyEightMg:
                        return NuclideViewModel.DecayType.Beta;
                    case RadiationDecayType.Mg:
                        return NuclideViewModel.DecayType.Beta;
                    case RadiationDecayType.Ne:
                        return NuclideViewModel.DecayType.NeutronEmission;
                    case RadiationDecayType.ThirtyFourSi:
                        return NuclideViewModel.DecayType.Beta;
                    case RadiationDecayType.SF:
                        return NuclideViewModel.DecayType.SpontaniousEmission;
                    case RadiationDecayType.BF:
                        return NuclideViewModel.DecayType.Beta;
                    case RadiationDecayType.EF:
                        return NuclideViewModel.DecayType.Beta;
                    case RadiationDecayType.Unknown:
                        return NuclideViewModel.DecayType.Stable;
                    default:
                        return NuclideViewModel.DecayType.Stable;
                }
            }
        }
    }
}
