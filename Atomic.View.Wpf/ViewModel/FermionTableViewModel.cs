﻿using Atomic.View.Wpf.Data.Interfaces;
using GalaSoft.MvvmLight;
using System.Collections.Generic;

namespace Atomic.View.Wpf.ViewModel
{
    public class FermionTableViewModel : ViewModelBase
    {
        IFermionDataService _data;

        public FermionTableViewModel(IFermionDataService data)
        {
            _data = data;

            QuarkTable = new NotifyTaskCompletion<IEnumerable<QuarkViewModel>>(_data.GetQuarkTableList());
            LeptonTable = new NotifyTaskCompletion<IEnumerable<LeptonViewModel>>(_data.GetLeptonTableList());
            BosonTable = new NotifyTaskCompletion<IEnumerable<BosonViewModel>>(_data.GetBosonTableList());
            HiggsBoson = new NotifyTaskCompletion<BosonViewModel>(_data.GetHiggsBoson());
        }

        public NotifyTaskCompletion<IEnumerable<QuarkViewModel>> QuarkTable { get; private set; }

        public NotifyTaskCompletion<IEnumerable<LeptonViewModel>> LeptonTable { get; private set; }

        public NotifyTaskCompletion<IEnumerable<BosonViewModel>> BosonTable { get; private set; }

        public NotifyTaskCompletion<BosonViewModel> HiggsBoson { get; private set; }

    }
}
