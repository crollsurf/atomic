﻿using System.Windows.Media;

namespace Atomic.View.Wpf.ViewModel
{
    public interface IFermionViewModel
    {
        RadialGradientBrush FermionColour { get; }

        double Charge { get; }
        string Name { get; }
        double Mass { get; }
        double Spin { get; }
        string Symbol { get; }
    }
}