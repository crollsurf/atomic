﻿using Atomic.Data;
using Atomic.View.Wpf.View.Elements;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls;
using System.Windows.Media;

namespace Atomic.View.Wpf.ViewModel
{
    public class NuclideViewModel : ViewModelBase
    {
        public enum DecayType { Stable, Alpha, Beta, NeutronEmission, ProtonEmission, SpontaniousEmission, PositronEmission, ElectronCapture};

        Nuclide _nuclide;
        Brush _nulcideColor;

        public NuclideViewModel(Nuclide data)
        {
            _nuclide = data;
            this.CloseWindowCommand = new RelayCommand<MetroWindow>(CloseWindow);
            this.DisplayDetailCommand = new RelayCommand(this.DisplayDetail, CanDisplayDetail);
        }

        #region Commands

        public RelayCommand DisplayDetailCommand { get; private set; }

        public bool CanDisplayDetail()
        {
            return _nuclide != null;
        }

        public void DisplayDetail()
        {
            //var detail = new LeptonView(this);
            //detail.Owner = Application.Current.MainWindow;
            //detail.Show();
        }

        public RelayCommand<MetroWindow> CloseWindowCommand { get; private set; }

        private void CloseWindow(MetroWindow window)
        {
            window.Close();
        }

        #endregion Commands

        #region Properties

        public double Abundance
        {
            get { return _nuclide.Abundance; }
        }

        public string Name
        {
            get { return _nuclide.Symbol + _nuclide.ProtonCount; }
        }

        public int? ElementId
        {
            get { return _nuclide.ElementId; }
        }

        public double Energy
        {
            get { return _nuclide.Energy; }
        }

        public double HalfLifeSeconds
        {
            get { return _nuclide.HalfLifeSeconds; }
        }

        public string HalfLifeText
        {
            get { return _nuclide.HalfLifeText; }
        }

        public double Mass
        {
            get { return _nuclide.Mass; }
        }

        public string JPi
        {
            get { return _nuclide.JPi; }
        }

        public int NeutronCount
        {
            get { return _nuclide.NeutronCount; }
        }

        public int ProtonCount
        {
            get { return _nuclide.ProtonCount; }
        }

        public string Symbol
        {
            get { return _nuclide.Symbol; }
        }

        #endregion Properties

        #region DisplayProperties

        public Brush NuclideColour
        {
            get
            {
                if (_nulcideColor == null)
                {
                    if (_nuclide.ElementId == null)
                        _nulcideColor = new SolidColorBrush(Colors.White);
                    else
                        _nulcideColor = ElementHelper.BackgroundColor(_nuclide.ElementId.Value);
                }

                return _nulcideColor;
            }
        }

        #endregion DisplayProperties
    }
}