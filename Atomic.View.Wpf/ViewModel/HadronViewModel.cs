﻿using Atomic.Data;
using Atomic.View.Wpf.Data.Interfaces;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls;
using System.Collections.Generic;
using System;
using System.Windows.Media;
using System.Windows;
using Atomic.View.Wpf.View.Hadrons;

namespace Atomic.View.Wpf.ViewModel
{
    public class HadronViewModel : ViewModelBase, IHadronViewModel
    {
        IHadronDataService _data;
        static RadialGradientBrush _quark1Color;
        static RadialGradientBrush _quark2Color;
        static RadialGradientBrush _quark3Color;
        static RadialGradientBrush _quarkBackColor;
        static RadialGradientBrush _hadronColor;
        static SolidColorBrush _lightHadronColor;


        public HadronViewModel(IHadronDataService data)
        {
            _data = data;

            this.CloseWindowCommand = new RelayCommand<MetroWindow>(CloseWindow);
            this.DisplayDetailCommand = new RelayCommand(this.DisplayDetail, CanDisplayDetail);
        }

        #region Commands

        public RelayCommand DisplayDetailCommand { get; private set; }

        public bool CanDisplayDetail()
        {
            return _data != null;
        }

        public void DisplayDetail()
        {
            var detail = new HadronView(this);
            detail.Owner = Application.Current.MainWindow;
            detail.Show();
        }

        public RelayCommand<MetroWindow> CloseWindowCommand { get; private set; }

        public string Name
        {
            get
            {
                return _data.Hadron.Name;
            }
        }

        public string Symbol
        {
            get
            {
                return _data.Hadron.Symbol;
            }
        }

        public string Family
        {
            get
            {
                return _data.Hadron.Family.ToString();
            }
        }

        public double MinimumMassMev
        {
            get
            {
                return _data.Hadron.MinimumMassMev;
            }
        }

        public double MaximumMassMev
        {
            get
            {
                return _data.Hadron.MaximumMassMev;
            }
        }

        public double Isospin
        {
            get
            {
                return _data.Hadron.Isospin;
            }
        }

        public double TotalAngularMomentum
        {
            get
            {
                return _data.Hadron.TotalAngularMomentum;
            }
        }

        public double Charge
        {
            get
            {
                return _data.Hadron.Charge;
            }
        }

        public int Strangeness
        {
            get
            {
                return _data.Hadron.Strangeness;
            }
        }

        public int Charm
        {
            get
            {
                return _data.Hadron.Charm;
            }
        }

        public int Bottomness
        {
            get
            {
                return _data.Hadron.Bottomness;
            }
        }

        public string MeanLifetime
        {
            get
            {
                return _data.Hadron.MeanLifetime;
            }
        }

        public string DecaysTo
        {
            get
            {
                return _data.Hadron.DecaysTo;
            }
        }

        public string Quark1Symbol
        {
            get
            {
                return _data.Hadron.Quark2Symbol;
            }
        }

        public string Quark2Symbol
        {
            get
            {
                return _data.Hadron.Quark2Symbol;
            }
        }

        public string Quark3Symbol
        {
            get
            {
                return _data.Hadron.Quark3Symbol;
            }
        }

        private void CloseWindow(MetroWindow window)
        {
            window.Close();
        }

        #endregion Commands

        #region Properties


        #endregion Properties

        #region DisplayProperties

        public RadialGradientBrush Quark1Colour
        {
            get
            {
                if (_quark1Color == null)
                {
                    _quark1Color = CreateRadialGradientBrush(Colors.Lime, Colors.LimeGreen, 0.5);
                    //_quark1Color = CreateRadialGradientBrush(Colors.Lime, Colors.DarkGray);
                }

                return _quark1Color;
            }
        }

        public RadialGradientBrush Quark2Colour
        {
            get
            {
                if (_quark2Color == null)
                {
                    //_quark2Color = CreateRadialGradientBrush(Colors.LightSalmon, Colors.DarkGray);
                    _quark2Color = CreateRadialGradientBrush(Colors.LightSalmon, Colors.Tomato, 0.5);
                }

                return _quark2Color;
            }
        }

        public RadialGradientBrush Quark3Colour
        {
            get
            {
                if (_quark3Color == null)
                {
                    _quark3Color = CreateRadialGradientBrush(Colors.Blue, Colors.LightBlue, 0.5);
                    //_quark3Color = CreateRadialGradientBrush(Colors.Blue, Colors.DarkGray);
                }

                return _quark3Color;
            }
        }

        public RadialGradientBrush QuarkBackColour
        {
            
            get
            {
                if (_quarkBackColor == null)
                {
                    _quarkBackColor = CreateRadialGradientBrush(Colors.DarkGray, Colors.LightSlateGray, 0.5);
                }

                return _quarkBackColor;

            }
        }

        private static RadialGradientBrush CreateRadialGradientBrush(Color color1, Color color2, double opacity = 1.0)
        {
            var radBrush = new RadialGradientBrush();
            radBrush.GradientOrigin = new Point(0.0, 0.0);
            radBrush.GradientStops.Add(new GradientStop(color1, 0.0));
            radBrush.GradientStops.Add(new GradientStop(color2, 1.0));
            radBrush.Opacity = opacity;

            return radBrush;
        }

        public RadialGradientBrush HadronColour
        {
            get
            {
                if (_hadronColor == null)
                {
                    _hadronColor = new RadialGradientBrush();
                    _hadronColor.GradientOrigin = new Point(0.0, 0.0);
                    _hadronColor.GradientStops.Add(new GradientStop(Colors.DarkGray, 0.0));
                    _hadronColor.GradientStops.Add(new GradientStop(Colors.LightSlateGray, 1.0));
                }

                return _hadronColor;
            }
        }

        public SolidColorBrush LightHadronColour
        {
            get
            {
                if (_lightHadronColor == null)
                {
                    _lightHadronColor = new SolidColorBrush(Colors.WhiteSmoke);
                }

                return _lightHadronColor;
            }
        }


        #endregion DisplyProperties

    }
}