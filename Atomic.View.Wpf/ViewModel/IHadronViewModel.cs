﻿using Atomic.Data;
using System.Collections.Generic;

namespace Atomic.View.Wpf.ViewModel
{
    public interface IHadronViewModel
    {
        string Name { get; }
        string Symbol { get; }
        string Family { get; }
        double MinimumMassMev { get; }
        double MaximumMassMev { get; }
        double Isospin { get; }
        double TotalAngularMomentum { get; }
        double Charge { get; }
        int Strangeness { get; }
        int Charm { get; }
        int Bottomness { get; }
        string MeanLifetime { get; }
        string DecaysTo { get; }
        string Quark1Symbol { get; }
        string Quark2Symbol { get; }
        string Quark3Symbol { get; }
    }
}