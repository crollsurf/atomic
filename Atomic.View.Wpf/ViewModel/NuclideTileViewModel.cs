﻿using Atomic.Data;
using Atomic.View.Wpf.Data.Interfaces;
using Atomic.View.Wpf.View.Elements;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Windows.Media;

namespace Atomic.View.Wpf.ViewModel
{
    public class NuclideTileViewModel : ViewModelBase
    {
        Nuclide _nuclide;

        public NuclideTileViewModel(INuclideDataService data)
        {
            _nuclide = data.Nuclide;
            Symbol = _nuclide.Symbol;
            ElementId = _nuclide.ElementId;
            NeutronCount = _nuclide.NeutronCount;

            this.DisplayDetailCommand = new RelayCommand(this.DisplayDetail);
        }

        #region Commands

        public RelayCommand DisplayDetailCommand { get; private set; }

        public void DisplayDetail()
        {
            //var detail = new LeptonView(this);
            //detail.Owner = Application.Current.MainWindow;
            //detail.Show();
        }

        #endregion Commands

        #region Properties

        public string Symbol { get; set; }
        public int? ElementId { get; set; }
        public int NeutronCount { get; set; }


        #endregion Properties

        #region DisplayProperties

        public SolidColorBrush NuclideColour
        {
            get
            {
                return (ElementId.HasValue) ? ElementHelper.BackgroundColor(ElementId.Value) : ElementHelper.TransparentBrush;
            }
        }

        #endregion DisplayProperties
    }
}