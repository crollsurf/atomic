﻿using Atomic.View.Wpf.Data.Interfaces;
using GalaSoft.MvvmLight;
using System.Collections.Generic;

namespace Atomic.View.Wpf.ViewModel
{
    public class PeriodicTableViewModel : ViewModelBase
    {
        IPeriodicDataService _data;

        public PeriodicTableViewModel(IPeriodicDataService data)
        {
            _data = data;
            PeriodicTable = new NotifyTaskCompletion<IEnumerable<IEnumerable<ElementViewModel>>>(_data.GetPeriodicTableList());
            SecondTable = new NotifyTaskCompletion<IEnumerable<IEnumerable<ElementViewModel>>>(_data.GetSecondTableList());
        }

        public NotifyTaskCompletion<IEnumerable<IEnumerable<ElementViewModel>>> PeriodicTable { get; private set; }

        public NotifyTaskCompletion<IEnumerable<IEnumerable<ElementViewModel>>> SecondTable { get; private set; }
    }
}
