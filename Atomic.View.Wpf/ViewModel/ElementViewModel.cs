﻿using Atomic.Data;
using Atomic.View.Wpf.Data.Interfaces;
using Atomic.View.Wpf.View.Elements;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using MahApps.Metro.Controls;
using System.Collections.Generic;
using System.Data.Entity;
using System.Windows;
using System.Windows.Media;

namespace Atomic.View.Wpf.ViewModel
{
    public class ElementViewModel : ViewModelBase
    {
        IElementDataService _data;
        Brush _backgroundColor;
        Brush _lighterBackgroundColor;
        Brush _textColor;
        bool _hasChanges;

        public ElementViewModel(IElementDataService data)
        {
            _data = data;
            this.CloseWindowCommand = new RelayCommand<MetroWindow>(CloseWindow);
            this.DisplayDetailCommand = new RelayCommand(this.DisplayDetail, CanDisplayDetail);
            base.PropertyChanged += ElementViewModel_PropertyChanged;     
        }

        private void ElementViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            _hasChanges = true;
        }

        #region Commands

        public RelayCommand DisplayDetailCommand { get; private set; }

        public bool CanDisplayDetail()
        {
            return _data.Element != null;
        }

        public void DisplayDetail()
        {
            var detail = new ElementView(this);
            detail.Owner = Application.Current.MainWindow;
            detail.Show();
        }

        public RelayCommand<MetroWindow> CloseWindowCommand { get; private set; }

        private void CloseWindow(MetroWindow window)
        {
            if (_hasChanges)
            {
                using (var ctx = new AtomicContext())
                {
                    ctx.Elements.Attach(_data.Element);
                    var element = ctx.Entry(_data.Element);
                    element.State = EntityState.Modified;

                    ctx.SaveChanges();
                }
            }

            window.Close();
        }

        #endregion Commands

        #region Properties

        public const string NamePropertyName = "Name";

        public string Name
        {
            get { return _data.Element.Name; }

            set
            {
                if (_data.Element.Name == value) return;

                _data.Element.Name = value;
                RaisePropertyChanged(() => Name);
            }
        }

        public const string NumberPropertyName = "Number";

        public int Number
        {
            get { return _data.Element.Number; }

            set
            {
                if (_data.Element.Number == value) return;

                _data.Element.Number = value;
                RaisePropertyChanged(() => Number);
            }
        }

        public const string SymbolPropertyName = "Symbol";

        public string Symbol
        {
            get { return _data.Element.Symbol; }

            set
            {
                if (_data.Element.Symbol == value) return;

                _data.Element.Symbol = value;
                RaisePropertyChanged(() => Symbol);
            }
        }

        public const string GroupPropertyName = "Group";

        public int Group
        {
            get { return _data.Element.Group; }

            set
            {
                if (_data.Element.Group == value) return;

                _data.Element.Group = value;
                RaisePropertyChanged(() => Group);
            }
        }

        public const string GroupOldPropertyName = "GroupOld";

        public string GroupOld
        {
            get { return _data.Element.GroupOld; }

            set
            {
                if (_data.Element.GroupOld == value) return;

                _data.Element.GroupOld = value;
                RaisePropertyChanged(() => GroupOld);
            }
        }

        public const string PeriodPropertyName = "Period";

        public int Period
        {
            get { return _data.Element.Period; }

            set
            {
                if (_data.Element.Period == value) return;

                _data.Element.Period = value;
                RaisePropertyChanged(() => Period);
            }
        }

        public const string NameFrenchPropertyName = "NameFrench";

        public string NameFrench
        {
            get { return _data.Element.NameFrench; }

            set
            {
                if (_data.Element.NameFrench == value) return;

                _data.Element.NameFrench = value;
                RaisePropertyChanged(() => NameFrench);
            }
        }

        public const string NameGermanPropertyName = "NameGerman";

        public string NameGerman
        {
            get { return _data.Element.NameGerman; }

            set
            {
                if (_data.Element.NameGerman == value) return;

                _data.Element.NameGerman = value;
                RaisePropertyChanged(() => NameGerman);
            }
        }

        public const string NameItalianPropertyName = "NameItalian";

        public string NameItalian
        {
            get { return _data.Element.NameItalian; }

            set
            {
                if (_data.Element.NameItalian == value) return;

                _data.Element.NameItalian = value;
                RaisePropertyChanged(() => NameItalian);
            }
        }

        public const string NameSpanishPropertyName = "NameSpanish";

        public string NameSpanish
        {
            get { return _data.Element.NameSpanish; }

            set
            {
                if (_data.Element.NameSpanish == value) return;

                _data.Element.NameSpanish = value;
                RaisePropertyChanged(() => NameSpanish);
            }
        }

        public const string CasRegistryNumberPropertyName = "CasRegistryNumber";

        public string CasRegistryNumber
        {
            get { return _data.Element.CasRegistryNumber; }

            set
            {
                if (_data.Element.CasRegistryNumber == value) return;

                _data.Element.CasRegistryNumber = value;
                RaisePropertyChanged(() => CasRegistryNumber);
            }
        }

        public const string UsesPropertyName = "Uses";

        public string Uses
        {
            get { return _data.Element.Uses; }

            set
            {
                if (_data.Element.Uses == value) return;

                _data.Element.Uses = value;
                RaisePropertyChanged(() => Uses);
            }
        }

        public const string EarthCrustPropertyName = "EarthCrust";

        public double? EarthCrust
        {
            get { return _data.Element.EarthCrust; }

            set
            {
                if (_data.Element.EarthCrust == value) return;

                _data.Element.EarthCrust = value;
                RaisePropertyChanged(() => EarthCrust);
            }
        }

        public const string SeawaterPropertyName = "Seawater";

        public double? Seawater
        {
            get { return _data.Element.Seawater; }

            set
            {
                if (_data.Element.Seawater == value) return;

                _data.Element.Seawater = value;
                RaisePropertyChanged(() => Seawater);
            }
        }

        public const string HumanBodyPropertyName = "HumanBody";

        public double? HumanBody
        {
            get { return _data.Element.HumanBody; }

            set
            {
                if (_data.Element.HumanBody == value) return;

                _data.Element.Seawater = value;
                RaisePropertyChanged(() => HumanBody);
            }
        }

        public const string ColorPropertyName = "Color";

        public string Color
        {
            get { return _data.Element.Color; }

            set
            {
                if (_data.Element.Color == value) return;

                _data.Element.Color = value;
                RaisePropertyChanged(() => Color);
            }
        }

        public const string YearDiscoveredPropertyName = "YearDiscovered";

        public int? YearDiscovered
        {
            get { return _data.Element.YearDiscovered; }

            set
            {
                if (_data.Element.YearDiscovered == value) return;

                _data.Element.YearDiscovered = value;
                RaisePropertyChanged(() => YearDiscovered);
            }
        }

        public const string RelativeAbundanceInSolarSystemPropertyName = "RelativeAbundanceInSolarSystem";

        public double? RelativeAbundanceInSolarSystem
        {
            get { return _data.Element.RelativeAbundanceInSolarSystem; }

            set
            {
                if (_data.Element.RelativeAbundanceInSolarSystem == value) return;

                _data.Element.RelativeAbundanceInSolarSystem = value;
                RaisePropertyChanged(() => RelativeAbundanceInSolarSystem);
            }
        }

        public const string AbundanceInEarthCrustPropertyName = "AbundanceInEarthCrust";

        public double? AbundanceInEarthCrust
        {
            get { return _data.Element.AbundanceInEarthCrust; }

            set
            {
                if (_data.Element.AbundanceInEarthCrust == value) return;

                _data.Element.AbundanceInEarthCrust = value;
                RaisePropertyChanged(() => AbundanceInEarthCrust);
            }
        }


        public const string MassPropertyName = "Mass";

        public double Mass
        {
            get { return _data.Element.Mass; }

            set
            {
                if (_data.Element.Mass == value) return;

                _data.Element.Mass = value;
                RaisePropertyChanged(() => Mass);
            }
        }

        public const string ChargePropertyName = "Charge";

        public double Charge
        {
            get { return _data.Element.Charge; }

            set
            {
                if (_data.Element.Charge == value) return;

                _data.Element.Charge = value;
                RaisePropertyChanged(() => Charge);
            }
        }

        public const string MeltingPointPropertyName = "MeltingPoint";

        public double? MeltingPoint
        {
            get { return _data.Element.MeltingPoint; }

            set
            {
                if (_data.Element.MeltingPoint == value) return;

                _data.Element.MeltingPoint = value;
                RaisePropertyChanged(() => MeltingPoint);
            }
        }

        public const string BoilingPointPropertyName = "BoilingPoint";

        public double? BoilingPoint
        {
            get { return _data.Element.BoilingPoint; }

            set
            {
                if (_data.Element.BoilingPoint == value) return;

                _data.Element.BoilingPoint = value;
                RaisePropertyChanged(() => BoilingPoint);
            }
        }

        public const string DensityPropertyName = "Density";

        public double? Density
        {
            get { return _data.Element.Density; }

            set
            {
                if (_data.Element.Density == value) return;

                _data.Element.Density = value;
                RaisePropertyChanged(() => Density);
            }
        }

        public const string ElectronegativityPropertyName = "Electronegativity";

        public double? Electronegativity
        {
            get { return _data.Element.Electronegativity; }

            set
            {
                if (_data.Element.Electronegativity == value) return;

                _data.Element.Electronegativity = value;
                RaisePropertyChanged(() => Electronegativity);
            }
        }


        public const string IonizationPotentialFirstPropertyName = "IonizationPotentialFirst";

        public double? IonizationPotentialFirst
        {
            get { return _data.Element.IonizationPotentialFirst; }

            set
            {
                if (_data.Element.IonizationPotentialFirst == value) return;

                _data.Element.IonizationPotentialFirst = value;
                RaisePropertyChanged(() => IonizationPotentialFirst);
            }
        }

        public const string IonizationPotentialSecondPropertyName = "IonizationPotentialSecond";

        public double? IonizationPotentialSecond
        {
            get { return _data.Element.IonizationPotentialSecond; }

            set
            {
                if (_data.Element.IonizationPotentialSecond == value) return;

                _data.Element.IonizationPotentialSecond = value;
                RaisePropertyChanged(() => IonizationPotentialSecond);
            }
        }

        public const string IonizationPotentialThirdPropertyName = "IonizationPotentialThird";

        public double? IonizationPotentialThird
        {
            get { return _data.Element.IonizationPotentialThird; }

            set
            {
                if (_data.Element.IonizationPotentialThird == value) return;

                _data.Element.IonizationPotentialThird = value;
                RaisePropertyChanged(() => IonizationPotentialThird);
            }
        }

        public const string ElectronAffinityPropertyName = "ElectronAffinity";

        public double? ElectronAffinity
        {
            get { return _data.Element.ElectronAffinity; }

            set
            {
                if (_data.Element.ElectronAffinity == value) return;

                _data.Element.ElectronAffinity = value;
                RaisePropertyChanged(() => ElectronAffinity);
            }
        }

        public const string PredictedElectronConfigurationPropertyName = "PredictedElectronConfiguration";

        public string PredictedElectronConfiguration
        {
            get { return _data.Element.PredictedElectronConfiguration; }

            set
            {
                if (_data.Element.PredictedElectronConfiguration == value) return;

                _data.Element.PredictedElectronConfiguration = value;
                RaisePropertyChanged(() => PredictedElectronConfiguration);
            }
        }

        public const string ObservedElectronConfigurationPropertyName = "ObservedElectronConfiguration";

        public string ObservedElectronConfiguration
        {
            get { return _data.Element.ObservedElectronConfiguration; }

            set
            {
                if (_data.Element.ObservedElectronConfiguration == value) return;

                _data.Element.ObservedElectronConfiguration = value;
                RaisePropertyChanged(() => ObservedElectronConfiguration);
            }
        }

        public const string AtomicRadiusPropertyName = "AtomicRadius";

        public double? AtomicRadius
        {
            get { return _data.Element.AtomicRadius; }

            set
            {
                if (_data.Element.AtomicRadius == value) return;

                _data.Element.AtomicRadius = value;
                RaisePropertyChanged(() => AtomicRadius);
            }
        }

        public const string IonicRadiusPropertyName = "IonicRadius";

        public double? IonicRadius
        {
            get { return _data.Element.IonicRadius; }

            set
            {
                if (_data.Element.IonicRadius == value) return;

                _data.Element.IonicRadius = value;
                RaisePropertyChanged(() => IonicRadius);
            }
        }

        public const string CovalentRadiusPropertyName = "CovalentRadius";

        public double? CovalentRadius
        {
            get { return _data.Element.CovalentRadius; }

            set
            {
                if (_data.Element.CovalentRadius == value) return;

                _data.Element.CovalentRadius = value;
                RaisePropertyChanged(() => CovalentRadius);
            }
        }

        public const string Radius1minusIonPropertyName = "Radius1minusIon";

        public int? Radius1minusIon
        {
            get { return _data.Element.Radius1minusIon; }

            set
            {
                if (_data.Element.Radius1minusIon == value) return;

                _data.Element.Radius1minusIon = value;
                RaisePropertyChanged(() => Radius1minusIon);
            }
        }

        public const string Radius2minusIonPropertyName = "Radius2minusIon";

        public int? Radius2minusIon
        {
            get { return _data.Element.Radius2minusIon; }

            set
            {
                if (_data.Element.Radius2minusIon == value) return;

                _data.Element.Radius2minusIon = value;
                RaisePropertyChanged(() => Radius2minusIon);
            }
        }

        public const string AtomicRadiusPmPropertyName = "AtomicRadiusPm";

        public double? AtomicRadiusPm
        {
            get { return _data.Element.AtomicRadiusPm; }

            set
            {
                if (_data.Element.AtomicRadiusPm == value) return;

                _data.Element.AtomicRadiusPm = value;
                RaisePropertyChanged(() => AtomicRadiusPm);
            }
        }

        public const string Radius1plusIonPropertyName = "Radius1plusIon";

        public double? Radius1plusIon
        {
            get { return _data.Element.Radius1plusIon; }

            set
            {
                if (_data.Element.Radius1plusIon == value) return;

                _data.Element.Radius1plusIon = value;
                RaisePropertyChanged(() => Radius1plusIon);
            }
        }

        public const string Radius2plusIonPropertyName = "Radius2plusIon";

        public double? Radius2plusIon
        {
            get { return _data.Element.Radius2plusIon; }

            set
            {
                if (_data.Element.Radius2plusIon == value) return;

                _data.Element.Radius2plusIon = value;
                RaisePropertyChanged(() => Radius2plusIon);
            }
        }

        public const string Radius3plusIonPropertyName = "Radius3plusIon";

        public double? Radius3plusIon
        {
            get { return _data.Element.Radius3plusIon; }

            set
            {
                if (_data.Element.Radius3plusIon == value) return;

                _data.Element.Radius3plusIon = value;
                RaisePropertyChanged(() => Radius3plusIon);
            }
        }

        public const string AtomicVolumePropertyName = "AtomicVolume";

        public double? AtomicVolume
        {
            get { return _data.Element.AtomicVolume; }

            set
            {
                if (_data.Element.AtomicVolume == value) return;

                _data.Element.AtomicVolume = value;
                RaisePropertyChanged(() => AtomicVolume);
            }
        }

        public const string CrystalStructurePropertyName = "CrystalStructure";

        public string CrystalStructure
        {
            get { return _data.Element.CrystalStructure; }

            set
            {
                if (_data.Element.CrystalStructure == value) return;

                _data.Element.CrystalStructure = value;
                RaisePropertyChanged(() => CrystalStructure);
            }
        }

        public const string ElectricalConductivityPropertyName = "ElectricalConductivity";

        public double? ElectricalConductivity
        {
            get { return _data.Element.ElectricalConductivity; }

            set
            {
                if (_data.Element.ElectricalConductivity == value) return;

                _data.Element.ElectricalConductivity = value;
                RaisePropertyChanged(() => ElectricalConductivity);
            }
        }

        public const string SpecificHeatPropertyName = "SpecificHeat";

        public double? SpecificHeat
        {
            get { return _data.Element.SpecificHeat; }

            set
            {
                if (_data.Element.SpecificHeat == value) return;

                _data.Element.SpecificHeat = value;
                RaisePropertyChanged(() => SpecificHeat);
            }
        }

        public const string HeatOfFusionPropertyName = "HeatOfFusion";

        public double? HeatOfFusion
        {
            get { return _data.Element.HeatOfFusion; }

            set
            {
                if (_data.Element.HeatOfFusion == value) return;

                _data.Element.HeatOfFusion = value;
                RaisePropertyChanged(() => HeatOfFusion);
            }
        }

        public const string HeatOfVaporizationPropertyName = "HeatOfVaporization";

        public double? HeatOfVaporization
        {
            get { return _data.Element.HeatOfVaporization; }

            set
            {
                if (_data.Element.HeatOfVaporization == value) return;

                _data.Element.HeatOfVaporization = value;
                RaisePropertyChanged(() => HeatOfVaporization);
            }
        }

        public const string ThermalConductivityPropertyName = "ThermalConductivity";

        public double? ThermalConductivity
        {
            get { return _data.Element.ThermalConductivity; }

            set
            {
                if (_data.Element.ThermalConductivity == value) return;

                _data.Element.ThermalConductivity = value;
                RaisePropertyChanged(() => ThermalConductivity);
            }
        }

        public const string StructurePropertyName = "Structure";

        public string Structure
        {
            get { return _data.Element.Structure; }

            set
            {
                if (_data.Element.Structure == value) return;

                _data.Element.Structure = value;
                RaisePropertyChanged(() => Structure);
            }
        }

        public const string HardnessPropertyName = "Hardness";

        public double? Hardness
        {
            get { return _data.Element.Hardness; }

            set
            {
                if (_data.Element.Hardness == value) return;

                _data.Element.Hardness = value;
                RaisePropertyChanged(() => Hardness);
            }
        }

        public const string ReactionWithAirPropertyName = "ReactionWithAir";

        public string ReactionWithAir
        {
            get { return _data.Element.ReactionWithAir; }

            set
            {
                if (_data.Element.ReactionWithAir == value) return;

                _data.Element.ReactionWithAir = value;
                RaisePropertyChanged(() => ReactionWithAir);
            }
        }

        public const string ReactionWithWaterPropertyName = "ReactionWithWater";

        public string ReactionWithWater
        {
            get { return _data.Element.ReactionWithWater; }

            set
            {
                if (_data.Element.ReactionWithWater == value) return;

                _data.Element.ReactionWithWater = value;
                RaisePropertyChanged(() => ReactionWithWater);
            }
        }

        public const string ReactionWith6MHClPropertyName = "ReactionWith6MHCl";

        public string ReactionWith6MHCl
        {
            get { return _data.Element.ReactionWith6MHCl; }

            set
            {
                if (_data.Element.ReactionWith6MHCl == value) return;

                _data.Element.ReactionWith6MHCl = value;
                RaisePropertyChanged(() => ReactionWith6MHCl);
            }
        }

        public const string ReactionWith15MHNO3PropertyName = "ReactionWith15MHNO3";

        public string ReactionWith15MHNO3
        {
            get { return _data.Element.ReactionWith15MHNO3; }

            set
            {
                if (_data.Element.ReactionWith15MHNO3 == value) return;

                _data.Element.ReactionWith15MHNO3 = value;
                RaisePropertyChanged(() => ReactionWith15MHNO3);
            }
        }

        public const string ReactionWith6MNaOHPropertyName = "ReactionWith6MNaOH";

        public string ReactionWith6MNaOH
        {
            get { return _data.Element.ReactionWith6MNaOH; }

            set
            {
                if (_data.Element.ReactionWith6MNaOH == value) return;

                _data.Element.ReactionWith6MNaOH = value;
                RaisePropertyChanged(() => ReactionWith6MNaOH);
            }
        }

        public const string HydridePropertyName = "Hydride";

        public string Hydride
        {
            get { return _data.Element.Hydride; }

            set
            {
                if (_data.Element.Hydride == value) return;

                _data.Element.Hydride = value;
                RaisePropertyChanged(() => Hydride);
            }
        }

        public const string OxidePropertyName = "Oxide";

        public string Oxide
        {
            get { return _data.Element.Oxide; }

            set
            {
                if (_data.Element.Oxide == value) return;

                _data.Element.Oxide = value;
                RaisePropertyChanged(() => Oxide);
            }
        }

        public const string ChloridePropertyName = "Chloride";

        public string Chloride
        {
            get { return _data.Element.Chloride; }

            set
            {
                if (_data.Element.Chloride == value) return;

                _data.Element.Chloride = value;
                RaisePropertyChanged(() => Chloride);
            }
        }

        public const string PolarizabilityPropertyName = "Polarizability";

        public double? Polarizability
        {
            get { return _data.Element.Polarizability; }

            set
            {
                if (_data.Element.Polarizability == value) return;

                _data.Element.Polarizability = value;
                RaisePropertyChanged(() => Polarizability);
            }
        }

        public const string HeatAtomizationPropertyName = "HeatAtomization";

        public int? HeatAtomization
        {
            get { return _data.Element.HeatAtomization; }

            set
            {
                if (_data.Element.HeatAtomization == value) return;

                _data.Element.HeatAtomization = value;
                RaisePropertyChanged(() => HeatAtomization);
            }
        }

        public const string IsotopesPropertyName = "Isotopes";

        public ICollection<Nuclide> Isotopes
        {
            get { return _data.Element.Isotopes; }

            set
            {
                if (_data.Element.Isotopes == value) return;

                _data.Element.Isotopes = value;
                RaisePropertyChanged(() => Isotopes);
            }
        }

        #endregion Properties

        #region DisplayProperties

        public Brush BackgroundColor
        {
            get
            {
                if (_backgroundColor == null)
                    _backgroundColor = (_data.Element == null) ? Brushes.Transparent : ElementHelper.BackgroundColor(_data.Element);

                return _backgroundColor;
            }
        }

        public Brush LighterBackgroundColor
        {
            get
            {
                if (_lighterBackgroundColor == null)
                {
                    if (_data.Element == null)
                    {
                        _lighterBackgroundColor = Brushes.Transparent;
                    }
                    else
                    {
                        Color color = ElementHelper.BackgroundColor(_data.Element).Color;
                        byte r = (byte)(color.R + ((255 - color.R) / 1.5));
                        byte g = (byte)(color.G + ((255 - color.G) / 1.5));
                        byte b = (byte)(color.B + ((255 - color.B) / 1.5));

                        Color lighter = System.Windows.Media.Color.FromRgb(r, g, b);
                        _lighterBackgroundColor = new SolidColorBrush(lighter);
                    }
                }

                return _lighterBackgroundColor;
            }
        }

        public Brush TextColor
        {
            get
            {
                if (_textColor == null)
                {
                     _textColor = Brushes.Black;
                }

                return _textColor;
            }

        }

        public Brush BorderBrush
        {
            get
            {
                return (_data.Element == null) ? Brushes.Transparent : Brushes.Black;
            }
        }

        public Thickness BorderThickness
        {
            get
            {
                return (_data.Element == null) ? new Thickness(0) : new Thickness(1);
            }
        }

         #endregion DisplyProperties
    }

}