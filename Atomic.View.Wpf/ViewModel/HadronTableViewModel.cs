﻿using Atomic.View.Wpf.Data.Interfaces;
using GalaSoft.MvvmLight;
using System.Collections.Generic;

namespace Atomic.View.Wpf.ViewModel
{
    public class HadronTableViewModel : ViewModelBase
    {
        IHadronTableDataService _data;

        public HadronTableViewModel(IHadronTableDataService data)
        {
            _data = data;
            BayronTable = new NotifyTaskCompletion<IEnumerable<HadronViewModel>>(_data.GetBaryonList());
            MesonTable = new NotifyTaskCompletion<IEnumerable<HadronViewModel>>(_data.GetMesonList());
        }

        public NotifyTaskCompletion<IEnumerable<HadronViewModel>> BayronTable { get; private set; }

        public NotifyTaskCompletion<IEnumerable<HadronViewModel>> MesonTable { get; private set; }
    }
}
