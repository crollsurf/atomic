﻿using Atomic.View.Wpf.Data;
using Atomic.View.Wpf.Data.Designer;
using Atomic.View.Wpf.Data.Interfaces;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;

namespace Atomic.View.Wpf.ViewModel
{
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            if (ViewModelBase.IsInDesignModeStatic)
            {
                SimpleIoc.Default.Register<IPeriodicDataService, DesignPeriodicDataService>();
                SimpleIoc.Default.Register<IFermionDataService, DesignFermionDataService>();
                SimpleIoc.Default.Register<INuclideDataService, DesignNuclideDataService>();
                SimpleIoc.Default.Register<IHadronTableDataService, DesignHadronTableDataService>();

                SimpleIoc.Default.Register<IElementDataService, DesignElementDataService>();
                SimpleIoc.Default.Register<IQuarkDataService, DesignQuarkDataService>();
                SimpleIoc.Default.Register<ILeptonDataService, DesignLeptonDataService>();
                SimpleIoc.Default.Register<IBosonDataService, DesignBosonDataService>();
                SimpleIoc.Default.Register<IHadronDataService, DesignHadronDataService>();
            }
            else
            {
                SimpleIoc.Default.Register<IPeriodicDataService, PeriodicDataService>();
                SimpleIoc.Default.Register<IFermionDataService, FermionDataService>();
                SimpleIoc.Default.Register<INuclideDataService, NuclideDataService>();
                SimpleIoc.Default.Register<IHadronTableDataService, HadronTableDataService>();

                SimpleIoc.Default.Register<IElementDataService, ElementDataService>();
                SimpleIoc.Default.Register<IQuarkDataService, QuarkDataService>();
                SimpleIoc.Default.Register<ILeptonDataService, LeptonDataService>();
                SimpleIoc.Default.Register<IBosonDataService, BosonDataService>();
                SimpleIoc.Default.Register<IHadronDataService, HadronDataService>();
            }

            SimpleIoc.Default.Register<MainWindow>();
            SimpleIoc.Default.Register<PeriodicTableViewModel>();
            SimpleIoc.Default.Register<FermionTableViewModel>();
            SimpleIoc.Default.Register<NuclideDataViewModel>();
            SimpleIoc.Default.Register<HadronTableViewModel>();

            SimpleIoc.Default.Register<ElementViewModel>();
            SimpleIoc.Default.Register<QuarkViewModel>();
            SimpleIoc.Default.Register<LeptonViewModel>();
            SimpleIoc.Default.Register<BosonViewModel>();
            SimpleIoc.Default.Register<NuclideTileViewModel>();
            SimpleIoc.Default.Register<HadronViewModel>();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public PeriodicTableViewModel PeriodicTable
        {
            get
            {
                return ServiceLocator.Current.GetInstance<PeriodicTableViewModel>();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public FermionTableViewModel FermionTable
        {
            get
            {
                return ServiceLocator.Current.GetInstance<FermionTableViewModel>();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public NuclideDataViewModel NuclideTable
        {
            get
            {
                return ServiceLocator.Current.GetInstance<NuclideDataViewModel>();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public HadronTableViewModel HadronTable
        {
            get
            {
                return ServiceLocator.Current.GetInstance<HadronTableViewModel>();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public ElementViewModel Element
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ElementViewModel>();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public QuarkViewModel Quark
        {
            get
            {
                return ServiceLocator.Current.GetInstance<QuarkViewModel>();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public LeptonViewModel Lepton
        {
            get
            {
                return ServiceLocator.Current.GetInstance<LeptonViewModel>();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public BosonViewModel Boson
        {
            get
            {
                return ServiceLocator.Current.GetInstance<BosonViewModel>();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public NuclideTileViewModel NuclideTile
        {
            get
            {
                return ServiceLocator.Current.GetInstance<NuclideTileViewModel>();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public HadronViewModel Hadron
        {
            get
            {
                return ServiceLocator.Current.GetInstance<HadronViewModel>();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
             "CA1822:MarkMembersAsStatic",
             Justification = "This non-static member is needed for data binding purposes.")]
        public HadronViewModel Meson
        {
            get
            {
                return ServiceLocator.Current.GetInstance<HadronViewModel>();
            }
        }


    }
}