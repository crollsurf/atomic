﻿using Atomic.View.Wpf.Data.Interfaces;
using Atomic.View.Wpf.View.Fermions;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls;
using System.Windows;
using System.Windows.Media;

namespace Atomic.View.Wpf.ViewModel
{
    public class QuarkViewModel : ViewModelBase, IFermionViewModel
    {
        IQuarkDataService _data;
        static RadialGradientBrush _fermionColor;

        public QuarkViewModel(IQuarkDataService data)
        {
            _data = data;
            this.CloseWindowCommand = new RelayCommand<MetroWindow>(CloseWindow);
            this.DisplayDetailCommand = new RelayCommand(this.DisplayDetail, CanDisplayDetail);
        }

        #region Commands

        public RelayCommand DisplayDetailCommand { get; private set; }

        public bool CanDisplayDetail()
        {
            return _data.Quark != null;
        }

        public void DisplayDetail()
        {
            var detail = new QuarkView(this);
            detail.Owner = Application.Current.MainWindow;
            detail.Show();
        }

        public RelayCommand<MetroWindow> CloseWindowCommand { get; private set; }

        private void CloseWindow(MetroWindow window)
        {
            window.Close();
        }

        #endregion Commands

        #region Properties

        public double BaryonNumber
        {
            get { return _data.Quark.BaryonNumber; }
        }

        public double Charge
        {
            get { return _data.Quark.Charge; }
        }

        public string Name
        {
            get { return _data.Quark.Flavor.ToString(); }
        }

        public double Mass
        {
            get { return _data.Quark.MaximumMassMev; }
        }

        public double MinimumMassMev
        {
            get { return _data.Quark.MinimumMassMev; }
        }

        public double Spin
        {
            get { return _data.Quark.Spin; }
        }

        public string Symbol
        {
            get { return _data.Quark.Symbol; }
        }

        #endregion Properties

        #region DisplayProperties

        public RadialGradientBrush FermionColour
        {
            get
            {
                if (_fermionColor == null)
                {
                    _fermionColor = new RadialGradientBrush();
                    _fermionColor.GradientOrigin = new Point(0.0, 0.0);
                    _fermionColor.GradientStops.Add(new GradientStop(Colors.Plum, 0.0));
                    _fermionColor.GradientStops.Add(new GradientStop(Colors.PaleVioletRed, 1.0));
                }

                return _fermionColor;
            }
        }

        #endregion DisplyProperties
    }
}