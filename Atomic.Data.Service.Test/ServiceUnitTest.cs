﻿using Atomic.Data.WebService;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace Atomic.Data.Service.Test
{
    /// <summary>
    /// Unit Tests to validate WCF communications with Atomic.Data
    /// </summary>
    [TestClass]
    public class ServiceUnitTest
    {
        [TestMethod]
        [TestCategory("Unit Test")]
        public void Mock_ServiceData_GetElementList()
        {
            AtomicService service = CreateElementDataAndReturnService();
            var elementList = service.GetElementList();

            Assert.AreEqual(118, elementList.Count(), "Expected 118 Elements");
        }

        [TestMethod]
        [TestCategory("Unit Test")]
        public void Mock_ServiceData_GetHydrogenElement()
        {
            AtomicService service = CreateElementDataAndReturnService();
            var element = service.GetElement("H");

            Assert.IsNotNull(element, "Expected a Hyrdogen Element");
        }

        AtomicService CreateElementDataAndReturnService()
        {
            var elements = Migrations.Configuration.GetSeedElements();
            var set = A.Fake<DbSet<Element>>(o => o.Implements(typeof(IQueryable<Element>)).Implements(typeof(IDbAsyncEnumerable<Element>)))
                        .SetupData(elements);

            var context = A.Fake<AtomicContext>();
            A.CallTo(() => context.Elements).Returns(set);

            return new AtomicService(context);
        }

        [TestMethod]
        [TestCategory("Unit Test")]
        public void Mock_ServiceData_GetHadronList()
        {
            AtomicService service = CreateHardronDataAndReturnService();
            var hadronList = service.GetHadronList();

            Assert.AreEqual(72, hadronList.Count, "Expected at least 72 Hadrons");
        }

        [TestMethod]
        [TestCategory("Unit Test")]
        public void Mock_ServiceData_GetHadron()
        {
            AtomicService service = CreateHardronDataAndReturnService();
            var element = service.GetHadron("Λ0");

            Assert.IsNotNull(element, "Expected Lambda[9] Hadron");
        }

        AtomicService CreateHardronDataAndReturnService()
        {
            var quarks = Migrations.Configuration.GetSeedQuarks();
            var hadrons = Migrations.Configuration.GetSeedHadrons(quarks);
            var set = A.Fake<DbSet<Hadron>>(o => o.Implements(typeof(IQueryable<Hadron>)).Implements(typeof(IDbAsyncEnumerable<Hadron>)))
                        .SetupData(hadrons);

            var context = A.Fake<AtomicContext>();
            A.CallTo(() => context.Hadrons).Returns(set);

            return new AtomicService(context); ;
        }

        [TestCategory("Unit Test")]
        [TestMethod]
        public void Mock_ServiceData_GetLeptonList()
        {
            AtomicService service = CreateLeptonDataAndReturnService();
            var leptonList = service.GetLeptonList();

            Assert.AreEqual(12, leptonList.Count, "Expected at least 12 Leptons");
        }

        [TestMethod]
        [TestCategory("Unit Test")]
        public void Mock_ServiceData_GetLepton()
        {
            AtomicService service = CreateLeptonDataAndReturnService();
            var lepton = service.GetLepton("e−");

            Assert.IsNotNull(lepton, "Expected Electron as a type of Lepton");
        }

        AtomicService CreateLeptonDataAndReturnService()
        {
            var leptons = Migrations.Configuration.GetSeedLeptons();
            var set = A.Fake<DbSet<Lepton>>(o => o.Implements(typeof(IQueryable<Lepton>)).Implements(typeof(IDbAsyncEnumerable<Lepton>)))
                        .SetupData(leptons);

            var context = A.Fake<AtomicContext>();
            A.CallTo(() => context.Leptons).Returns(set);

            return new AtomicService(context); ;
        }

        [TestMethod]
        [TestCategory("Unit Test")]
        public void Mock_ServiceData_GetMoleculeList()
        {
            AtomicService service = CreateMoleculeDataAndReturnService();
            var moleculeList = service.GetMoleculeList();

            Assert.AreEqual(139, moleculeList.Count, "Expected at least 139 Molecules");
        }

        [TestMethod]
        [TestCategory("Unit Test")]
        public void Mock_ServiceData_GetMolecule()
        {
            AtomicService service = CreateMoleculeDataAndReturnService();
            var molecule = service.GetMolecule("Aluminum monochloride");

            Assert.IsNotNull(molecule, "Expected Molecule type Aluminum Monochloride");
        }

        AtomicService CreateMoleculeDataAndReturnService()
        {
            var molecules = Migrations.Configuration.GetSeedMolecules();
            var set = A.Fake<DbSet<Molecule>>(o => o.Implements(typeof(IQueryable<Molecule>)).Implements(typeof(IDbAsyncEnumerable<Molecule>)))
                        .SetupData(molecules);

            var context = A.Fake<AtomicContext>();
            A.CallTo(() => context.Molecules).Returns(set);

            return new AtomicService(context); ;
        }

        [TestMethod]
        [TestCategory("Unit Test")]
        public void Mock_ServiceData_GetQuarkList()
        {
            AtomicService service = CreateQuarkDataAndReturnService();
            var quarkList = service.GetQuarkList();

            Assert.AreEqual(12, quarkList.Count, "Expected at least 12 Quarks");
        }

        [TestMethod]
        [TestCategory("Unit Test")]
        public void Mock_ServiceData_GetQuark()
        {
            AtomicService service = CreateQuarkDataAndReturnService();
            var quark = service.GetQuark("u");

            Assert.IsNotNull(quark, "Expected Quark of type Up");
        }

        AtomicService CreateQuarkDataAndReturnService()
        {
            var quarks = Migrations.Configuration.GetSeedQuarks();
            var set = A.Fake<DbSet<Quark>>(o => o.Implements(typeof(IQueryable<Quark>)).Implements(typeof(IDbAsyncEnumerable<Quark>)))
                        .SetupData(quarks);

            var context = A.Fake<AtomicContext>();
            A.CallTo(() => context.Quarks).Returns(set);

            return new AtomicService(context); ;
        }

    }
}
