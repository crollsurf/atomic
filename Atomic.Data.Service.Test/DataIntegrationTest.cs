﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Atomic.Data.Service.Test
{
    [TestClass]
    public class DataIntegrationTest
    {
        [TestCategory("IntegrationTest")]
        [TestMethod]
        public void Integration_Data_Returned_At_Least_118_Elements()
        {
            using (var ctx = new AtomicContext())
            {
                Assert.IsTrue(ctx.Elements.Count() >= 118);
            }
        }

        [TestCategory("IntegrationTest")]
        [TestMethod]
        public void Integration_Data_Returned_At_Least_72_Hadrons()
        {
            using (var ctx = new AtomicContext())
            {
                Assert.IsTrue(ctx.Hadrons.Count() >= 72);
            }
        }

        [TestCategory("IntegrationTest")]
        [TestMethod]
        public void Integration_Data_Returned_At_Least_12_Leptons()
        {
            using (var ctx = new AtomicContext())
            {
                Assert.IsTrue(ctx.Leptons.Count() >= 12);
            }
        }

        [TestCategory("IntegrationTest")]
        [TestMethod]
        public void Integration_Data_Returned_At_Least_139_Molecules()
        {
            using (var ctx = new AtomicContext())
            {
                Assert.IsTrue(ctx.Molecules.Count() >= 139);
            }
        }

        [TestCategory("IntegrationTest")]
        [TestMethod]
        public void Integration_Data_Returned_At_Least_12_Quarks()
        {
            using (var ctx = new AtomicContext())
            {
                Assert.IsTrue(ctx.Quarks.Count() >= 12);
            }
        }

        [TestCategory("IntegrationTest")]
        [TestMethod]
        public void Integration_Data_Returned_Carbon_Nuclides()
        {
            using (var ctx = new AtomicContext())
            {
                Assert.IsTrue(ctx.Nuclides.Where(e => e.Symbol == "C").Count() > 0);
            }
        }
    }
}
