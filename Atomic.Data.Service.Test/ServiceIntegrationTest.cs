﻿using Atomic.Data.Service.Test.AtomicDataServiceReference;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Atomic.Data.Service.Test
{
    [TestClass]
	public class ServiceIntegrationTest
	{
		[TestCategory("IntegrationTest")]
		[TestMethod]
		public void Integration_Service_Returned_At_Least_118_Elements()
		{
			using (AtomicServiceClient client = new AtomicServiceClient())
			{
				Assert.IsTrue(client.GetElementList().Length >= 118);
			}
		}

		[TestCategory("IntegrationTest")]
		[TestMethod]
		public void Integration_Service_Returned_At_Least_72_Hadrons()
		{
			using (AtomicServiceClient client = new AtomicServiceClient())
			{
				Assert.IsTrue(client.GetHadronList().Length >= 72);
			}
		}

		[TestCategory("IntegrationTest")]
		[TestMethod]
		public void Integration_Service_Returned_At_Least_12_Leptons()
		{
			using (AtomicServiceClient client = new AtomicServiceClient())
			{
				Assert.IsTrue(client.GetLeptonList().Length >= 12);
			}
		}

		[TestCategory("IntegrationTest")]
		[TestMethod]
		public void Integration_Service_Returned_At_Least_139_Molecules()
		{
			using (AtomicServiceClient client = new AtomicServiceClient())
			{
				Assert.IsTrue(client.GetMoleculeList().Length >= 139);
			}
		}

		[TestCategory("IntegrationTest")]
		[TestMethod]
		public void Integration_Service_Returned_At_Least_12_Quarks()
		{
			using (AtomicServiceClient client = new AtomicServiceClient())
			{
                Assert.IsTrue(client.GetQuarkList().Length >= 12);
			}
		}

		[TestCategory("IntegrationTest")]
		[TestMethod]
		public void Integration_Service_Returned_Carbon_Nuclides()
		{
			using (AtomicServiceClient client = new AtomicServiceClient())
			{
				Assert.IsTrue(client.GetNulcides("C").Length > 0);
			}
		}
	}
}
